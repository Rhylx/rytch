# Norme C++

## Gestion des fichiers:
- définir une seule classe par fichier
- utiliser #include "nom_classe.hpp" pour les classes internes
- utiliser #include <NomClasse> pour les classes externes
- définir les méthodes dans l'ordre où elles sont déclarées

## Commentaires:
- commentaires d'une ligne max. dans les définitions de fonctions
- commentaires descriptif avant la définition

## Indentation:
- tab = 2 espaces
- indentation pour chaque création de scope
- pas de retour à la ligne lors de l'ouverture du scope

## Nomenclature:
- les classes ou structures ou enums commencent toujours par une lettre majuscule
- les méthodes doivent commencer par une lettre minuscule
- lorsqu'un nom de méthode est formée par un mot composé, la première lettre des autres mots sont en majuscule
- autant que possible, un nom de méthode ou de fonction doit être de la forme <verbe+complément> et exprimer clairement ce que fait la méthode ou la fonction
- les attributs doivent commencer par une lettre minuscule et lorsqu'un attribut est composé de plus d'un mot, chaque première lettre des autres mots sont en lettres majuscules
- les attributs privés doivent commencer par "_"
- les constexpr sont en majuscules avec un "_" pour séparer les mots composés

## Méthodes:
- une méthode qui n'affecte pas l'état d'une classe doit être déclarée const
- une méthode qui est un override doit être systématiquement déclarée override

## Classes:
- les membres publics commencent par le constructeur et destructeur
- commencer dans l'ordre par les membres publics, les publics slots, les signaux, les protects slots, les protects, les privates slots, les méthodes  privées, les attributs privés
- séparer chaque bloc avec une ligne vide
- séparer "Q_OBJECT" d'une ligne vide avant et après
- dans les headers files préférer les forward aux includes

## Includes:
- commencer par les includes externes sauf dans les .cpp et séparer includes externes et internes par une ligne vide
- mettre le plus possible d'includes dans le cpp et le moins possible dans le hpp

## Syntaxe:
- mettre un espace avant et après un signe "="
- supprimer les espaces inutiles
- mettre un espace avant et après un accesseur ":"
- autant que possible privilégier l'initialisateur liste
- aller à la ligne pour chaque attribut initialisé
- initialisation liste commence par un retour à la ligne
- s'il y a une initialisation liste scope "{" du scope est sur une nouvelle ligne
