#ifndef MESSAGE_STRUCT_HPP
#define MESSAGE_STRUCT_HPP

#include <QString>

enum msgRole {
  body = 0x0101,
  fromSelf,
};

struct MessageStruct
{
  int messageId;
  QString senderJid;
  QString recpJid;
  int chatId;
  int64_t dateSent;
  int64_t dateRecv;
  QString body;
};

#endif // CHAT_PARAM_HPP
