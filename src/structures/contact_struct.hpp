#ifndef CONTACT_STRUCT_HPP_INCLUDED
#define CONTACT_STRUCT_HPP_INCLUDED

#include <QMetaType>
#include <QString>

namespace ContactEnum {
  enum SubscriptionStatus {
    None = 0b00000000,
    From = 0b00000001, // When the contact is subscribed to the user
    To   = 0b00000010, // When the user is subscribed to the contact
    Both = 0b00000011
  };
  enum contactRole {
    name = 0x0101,
    jid,
    inroster,
    status
  };
  enum requestAction {
    Accept,
    Deny
  };
  enum AvailableStatus {
    Online,
    Offline,
    Away,
    XA,
    DND,
    Chat,
    Unknown
  };
}

struct ContactStruct {
  QString jid;
  QString name;
  ContactEnum::SubscriptionStatus inroster = ContactEnum::None;
  ContactEnum::AvailableStatus status = ContactEnum::Unknown;
};

Q_DECLARE_METATYPE(ContactEnum::SubscriptionStatus)
Q_DECLARE_METATYPE(ContactEnum::AvailableStatus)
Q_DECLARE_METATYPE(ContactStruct)

#endif
