#ifndef CHAT_STRUCT_HPP
#define CHAT_STRUCT_HPP

#include <QStringView>

#include "../models/chat_model.hpp"

class ChatUi;

enum chatRole {
  label = 0x0101,
  uiP
};

struct ChatStruct
{
  int chatId;
  QString chatLabel;
  QString recpJid;
  std::shared_ptr<ChatUi> chatUi; // Can't move unique_ptr
  std::shared_ptr<ChatModel> chatM;
};

#endif // CHAT_PARAM_HPP
