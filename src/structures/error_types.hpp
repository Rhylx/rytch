#ifndef ERROR_TYPE_INCLUDED
#define ERROR_TYPE_INCLUDED

class Missing : public std::runtime_error {
  using std::runtime_error::runtime_error;
};

class ArgNotInEnum : public std::runtime_error {
  using std::runtime_error::runtime_error;
};

class DiskError : public std::runtime_error {
  using std::runtime_error::runtime_error;
};


#endif
