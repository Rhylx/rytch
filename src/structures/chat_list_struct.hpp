#ifndef CHAT_MODEL_STRUCT_HPP
#define CHAT_MODEL_STRUCT_HPP

#include "../core/xmpp_sender.hpp"

class SqliteHandler;

struct ChatListStruct
{
  int chatId;
  QString myJid;
  QString recpJid;
  std::shared_ptr<XmppSender> sender;
  std::shared_ptr<SqliteHandler> sqlite;
};

#endif
