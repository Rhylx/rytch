#include "login_ui.hpp"
#include "../styles/global_style.hpp"
#include "../core/logger.hpp"

#include <QPainter>
#include <QPalette>
#include <QPen>
#include <QSizePolicy>
#include <QStandardPaths>

#include <fstream>
#include <filesystem>

LoginUi::LoginUi(QWidget *parent) :
  QWidget(parent),
  _jidField(new RoundLineEdit(this)),
  _passwdField(new RoundLineEdit(this)),
  _loginBt(new RoundButton(this))
{
  setupUi();
  this->setGeometry(parent->geometry());
  fetchLogin();
  connect(_loginBt, &QPushButton::clicked, this, &LoginUi::setCreds);
  connect(_loginBt, &QPushButton::clicked, this, &LoginUi::connectBtClicked);
}

void LoginUi::setupUi() {
  this->setAutoFillBackground(true);
  QPalette loginPal;
  loginPal.setColor(QPalette::Window, Style::LoginStyle::LoginUi.backgroundColor);
  this->setPalette(loginPal);
  // set size:
  float r_width=this->size().width()/20.;
  float r_height=this->size().height()/12.;
  // jid field :
  QPalette jidCredPalette;
  jidCredPalette.setColor(QPalette::Window, Style::LoginStyle::qLineEdit.CS.backgroundColor);
  jidCredPalette.setColor(QPalette::Normal, QPalette::WindowText, Style::LoginStyle::qLineEdit.CS.textColor);
  _jidField->setAutoFillBackground(true);
  _jidField->setPalette(jidCredPalette);
  _jidField->resize(8*r_width,r_height);
  _jidField->move(6*r_width,3*r_height);
  //_jidField->setText(QString("alice@localhost"));
  _jidField->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Fixed);
  _jidField->setAlignment(Qt::AlignCenter);

  // passwd field :
  _passwdField->setPalette(jidCredPalette);
  _passwdField->resize(8*r_width,r_height);
  _passwdField->move(6*r_width,6*r_height);
  //_passwdField->setText(QString("alice"));
  _passwdField->setEchoMode(QLineEdit::Password);
  _passwdField->setAlignment(Qt::AlignCenter);
  // login button :
  QPalette loginBtPalette;
  loginBtPalette.setColor(QPalette::Button, Style::LoginStyle::qPushButton.CS.backgroundColor);
  loginBtPalette.setColor(QPalette::Normal, QPalette::WindowText, Style::LoginStyle::qPushButton.CS.textColor);
  _loginBt->setPalette(loginBtPalette);
  _loginBt->setRadius(Style::LoginStyle::qPushButton.SS.borderRadius);
  _loginBt->resize(3*r_width,r_height);
  _loginBt->move(16*r_width,8*r_height);
  _loginBt->setText(QString("Login"));
}

void LoginUi::setCreds() {
  QXmppConfiguration xmpp_creds;
  xmpp_creds.setJid(_jidField->text());
  xmpp_creds.setPassword(_passwdField->text());
//  xmpp_creds.setIgnoreSslErrors(true); // NEED TO REMOVED
  std::ofstream file(_logFile, std::ios::out | std::ios::trunc);
  if (file.is_open()) {
    file << _jidField->text().toStdString();
    file.close();
  } else {
    logger.log("[LoginUi] Failed to lastLogin file to update last login used",LogLevel::Warning);
  }
  emit sendXmppCreds(xmpp_creds);
}

void LoginUi::resizeEvent(QResizeEvent *event) {
  float r_width=size().width()/20.;
  float r_height=size().height()/12.;
  _jidField->resize(8*r_width,r_height);
  _jidField->move(6*r_width,3*r_height);
  _passwdField->resize(8*r_width,r_height);
  _passwdField->move(6*r_width,6*r_height);
  _loginBt->resize(3*r_width,r_height);
  _loginBt->move(16*r_width,8*r_height);
  QWidget::resizeEvent(event);
}

void LoginUi::fetchLogin() {
  const char* xdg_state_home = std::getenv("XDG_STATE_HOME");
  std::filesystem::path xdg_path;
  if (xdg_state_home == NULL) {
    xdg_path = std::filesystem::path((QStandardPaths::writableLocation(QStandardPaths::HomeLocation)).toStdString().c_str());
    xdg_path.append(".local");
    xdg_path.append("state");
    xdg_path.append("rytch");
  } else {
    std::filesystem::path xdg_path = std::filesystem::path(xdg_state_home);
    xdg_path.append("rytch");
  }
  std::filesystem::create_directories(xdg_path);
  xdg_path.append("lastLogin");
  _logFile = xdg_path.string();
  if (std::filesystem::exists(xdg_path)) {
    std::ifstream file(_logFile, std::ios::in);
    if (file.is_open()) {
      std::string lastLogin;
      std::getline(file,lastLogin);
      file.close();
      _jidField->setText(QString::fromStdString(lastLogin));
    } else {
      logger.log("[LoginUi] Failed to open lastLogin file to read the last login used",LogLevel::Warning);
      _jidField->setText(QString(""));
    }
  } else {
    _jidField->setText(QString(""));
  }
}

