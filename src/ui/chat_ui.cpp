#include "chat_ui.hpp"
#include "../styles/global_style.hpp"



//TODO REMOVEME
#include <iostream>

ChatUi::ChatUi(QWidget *parent, ChatModel *chatModel) :
  QWidget(parent)
{
  setupUi();
  _msgDelegate = std::make_unique<ChatDelegate>(_msgView);
  _msgView->setModel(chatModel);
  _msgView->setItemDelegate(_msgDelegate.get());
  _msgEdit->installEventFilter(this);
  connect(_msgEdit,&QPlainTextEdit::textChanged,
          this,&ChatUi::onTextChanged);
  connect(_sendB,&QPushButton::clicked,
          this,&ChatUi::sendB);
  connect(_returnB,&QPushButton::clicked,
          this,&ChatUi::returnB);
}

ChatUi::~ChatUi() {
  ;
}

void ChatUi::setupUi() {
  QPalette palette = this->palette();
  palette.setColor(QPalette::Window,Style::ChatStyle::ChatUi.backgroundColor);
  this->setPalette(palette);
  this->setAutoFillBackground(true);
  setGeometry(0,0,981,787);
  _chatCont = new QFrame(this);
  _chatCont->setGeometry(580,130,120,80);
  _chatCont->setFrameShape(QFrame::Shape::StyledPanel);
  _chatCont->setFrameShadow(QFrame::Shadow::Raised);
  _msgView = new ChatView(this);
  _msgView->setGeometry(-70,-20,231,131);
  _returnB = new ReturnButtonUi(this);
  _returnB->setGeometry(50,10,28,22);
  _returnB->setSizePolicy(QSizePolicy::Preferred,QSizePolicy::Fixed);
  _returnB->setText("...");
  _sendB = new RoundButton(this);
  _sendB->setRadius(Style::ChatStyle::sendB.SS.borderRadius);
  _sendB->setGeometry(540,410,80,23);
  _sendB->setText("Send");
  QPalette palsB;
  palsB.setColor(QPalette::Button,Style::ChatStyle::sendB.CS.backgroundColor);
  palsB.setColor(QPalette::Normal, QPalette::WindowText, Style::ChatStyle::sendB.CS.textColor);
  _sendB->setPalette(palsB);
  _msgEdit = new QPlainTextEdit(this);
  _msgEdit->setGeometry(320,150,321,381);
  _msgEdit->setSizePolicy(QSizePolicy::Ignored,QSizePolicy::Ignored);
}

void ChatUi::sendB() {
  QString rv = _msgEdit->toPlainText();
  _msgEdit->setPlainText("");
  if (rv.size() == 0) return;
  emit sendRequest(rv);
  return;
}

void ChatUi::returnB() {
  this->hide();
}

void ChatUi::onTextChanged() {
  int new_nblines = _msgEdit->document()->lineCount();
  if (new_nblines != _prevNblines) {
    _prevNblines = new_nblines;
    resizeEvent(nullptr);
  }
}

void ChatUi::resizeEvent(QResizeEvent *event) {
  int r_width = parentWidget()->size().width();
  int r_height = parentWidget()->size().height();
  this->resize(r_width,r_height);
  // Compute text requested height
  int fontHeight = QFontMetrics(_msgEdit->font()).height();
  int nblines = _msgEdit->document()->lineCount();
  int edit_height = fontHeight*(std::min(nblines,10) + 0.75); // Maximum 10 lines
  int sendB_height = 1.75*fontHeight; // minimum height of msgEdit
  double unit_w = r_width/80.;
  double unit_h = r_height/80.;
  int msgView_height = r_height - 5*unit_h - edit_height;
  // Resize objects
  _chatCont->resize(r_width,r_height);
  _msgView->resize(r_width - 4*unit_w,msgView_height);
  _msgEdit->resize(r_width - 14*unit_w,edit_height);
  _sendB->resize(9*unit_w,sendB_height);
  _returnB->resize(5*unit_w,5*unit_h);
  // Place objects
  _chatCont->move(0,0);
  _msgView->move(2*unit_w,2*unit_h);
  _msgEdit->move(2*unit_w,msgView_height + 3*unit_h);
  _sendB->move(69*unit_w,msgView_height + 3*unit_h + edit_height/2 - sendB_height/2);
  _returnB->move(2*unit_w,2*unit_h);
  // Mask returnB
  QRegion ell1(QRect(-2.5*unit_w,-2.5*unit_h,5*unit_w+1,5*unit_h+1),QRegion::Ellipse);
  _returnB->setMask(ell1);
  QWidget::resizeEvent(event);
}

void ChatUi::showEvent(QShowEvent *event) {
  if (!(event->spontaneous())) {
    // Set default, non spontaneous -> call by the application
    resizeEvent(nullptr);
  }
  QWidget::showEvent(event);
}

bool ChatUi::eventFilter(QObject *watched, QEvent *event) {
  if (watched == this->_msgEdit) {
    if (event->type() == QEvent::KeyPress) {
      QKeyEvent *keyEvent = static_cast<QKeyEvent*>(event);
      int key = keyEvent->key();
      if (!(keyEvent->modifiers() & Qt::ShiftModifier) &&
        (key == Qt::Key_Return || key == Qt::Key_Enter)) {
        this->sendB();
        return true;
      }
    }
    return false;
  } else {
    return QWidget::eventFilter(watched,event);
  }
}
