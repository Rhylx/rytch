#ifndef CREATE_CONTACT_UI_HPP
#define CREATE_CONTACT_UI_HPP

#include <QLineEdit>

#include "../styles/seamless_frame.hpp"

class CreateContactUi: public SeamlessFrame {

  Q_OBJECT

  public:
    CreateContactUi(QWidget *parent = nullptr);
    ~CreateContactUi() {;}

  signals:
    void ContactRequestSent(const QString &c_jid);

  private slots:
    void sendContactRequest();

  private:
    QLineEdit *_jidEdit;
};

#endif
