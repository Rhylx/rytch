#include "create_contact_ui.hpp"

#include <QPushButton>
#include <QVBoxLayout>

CreateContactUi::CreateContactUi(QWidget *parent) : SeamlessFrame(parent) {
  _jidEdit = new QLineEdit(this);
  _jidEdit->setPlaceholderText(tr("Contact jid"));
  QPushButton *send_request_b = new QPushButton(tr("Send Request"), this);
  connect(send_request_b, &QPushButton::clicked, this, &CreateContactUi::sendContactRequest);
  QVBoxLayout *layout = new QVBoxLayout(this);
  layout->addWidget(_jidEdit);
  layout->addWidget(send_request_b);

}

void CreateContactUi::sendContactRequest() {
  emit ContactRequestSent(_jidEdit->text());
  close();
}
