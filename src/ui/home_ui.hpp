#ifndef HOME_UI_HPP
#define HOME_UI_HPP

#include <QWidget>
#include <QStackedWidget>

#include "login_ui.hpp"
#include "chat_list_cont_ui.hpp"
#include "contact_list_cont_ui.hpp"
#include "../core/presence_handler.hpp"
#include "../core/client_err_handler.hpp"
#include "../models/chat_list_model.hpp"
#include "../models/contact_list_model.hpp"
#include "../views/contact_list_view.hpp"
#include "../delegates/contact_list_delegate.hpp"

class HomeUi: public QWidget {

  Q_OBJECT

  public:
    HomeUi(QWidget *parent = nullptr);
    ~HomeUi();

  private slots:
    void initDb(QXmppConfiguration const& xmpp_creds);
    void setHomeCurrentwidget();
    void manageCreateContact();
    void requestUpdated(const QModelIndex &parent, int first, int last);
    void showRequestDialogue(const QModelIndex &index);
    void popupRequestList(const QRect &basePos);
    void hideRequestList();
    void popupCreateChat(const QRect &basePos);
    void hideCreateChat();
    void addChatFromContact(const QModelIndex &index);

  private:
    void setupUi();
    void resizeEvent(QResizeEvent *event) override;
    void paintEvent(QPaintEvent *) override;
    // External
    std::shared_ptr<QXmppClient> _client;
    std::shared_ptr<SqliteHandler> _sqlite;
    std::shared_ptr<ClientErrHandler> _clientErr;
    // Models
    std::shared_ptr<ContactListModel> _contactList;
    std::shared_ptr<ContactListModel> _requestList;
    std::shared_ptr<ChatListModel> _chatManager;
    // Handlers
    std::unique_ptr<PresenceHandler> _presenceHandler;
    // Widgets
    QStackedWidget *_stackedWidget;
    LoginUi *_login;
    QWidget *_homePage;
    ChatListCont *_chatListCont;
    ContactListCont *_contactListCont;
    // Popups
    ContactListView *_requestListView,*_createChatContactView;
    std::unique_ptr<ContactListDelegate> _contactListDelegate;
};

#endif
