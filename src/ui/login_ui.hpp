#ifndef LOGIN_UI_HPP
#define LOGIN_UI_HPP

#include <QWidget>
#include <QLineEdit>
#include <QPushButton>
#include <QXmppConfiguration.h>
#include "../styles/roundbutton.hpp"
#include "../styles/roundlineedit.hpp"

class LoginUi : public QWidget {

  Q_OBJECT

  public:
    LoginUi(QWidget *parent = nullptr);
    ~LoginUi(){;}

  signals:
    void sendXmppCreds(QXmppConfiguration const& xmpp_creds);
    void connectBtClicked();

  private slots:
    void setCreds();

 private:
    void resizeEvent(QResizeEvent *event);
    void setupUi();
    void fetchLogin();
    RoundLineEdit* _jidField;
    RoundLineEdit* _passwdField;
    RoundButton* _loginBt;
    std::string _logFile; /// Contains the location of a file with the last login used (default to ~/.local/state/rytch/lastLogin)
};

#endif
