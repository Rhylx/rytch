#include "mainwindow.hpp"

#include <QCoreApplication>
#include <QResizeEvent>

MainWindow::MainWindow(QWidget *parent) :
  QMainWindow(parent),
  _centralWidget(new QWidget(this)),
  _homeUi(new HomeUi(this))
{
  setupUi();
}

MainWindow::~MainWindow() {
  delete _homeUi;
}

void MainWindow::setupUi() {
  setGeometry(0,0,851,731);
  setCentralWidget(this->_homeUi);
}

void MainWindow::resizeEvent(QResizeEvent *event) {
  QList<QWidget *> widgets = this->findChildren<QWidget *>(QString(), Qt::FindDirectChildrenOnly);
  for (int i = 0; i < widgets.size(); ++i) {
    if (!widgets[i]->isHidden()) {
      QResizeEvent * eventcp = new QResizeEvent(*event);
      QCoreApplication::postEvent(widgets[i],eventcp);
    }
  }
}
