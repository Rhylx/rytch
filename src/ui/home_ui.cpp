#include "home_ui.hpp"

#include <QVBoxLayout>
#include <QPainter>
#include <QPropertyAnimation>
#include <QMessageBox>
#include <QXmppRosterManager.h>
#include <QXmppMessageReceiptManager.h>

#include "../core/logger.hpp"
#include "create_contact_ui.hpp"
#include "../structures/error_types.hpp"
#include "../styles/global_style.hpp"

HomeUi::HomeUi(QWidget *parent) :
  QWidget(parent),
  _client(std::make_shared<QXmppClient>()),
  _sqlite(nullptr),
  _clientErr(std::make_shared<ClientErrHandler>()),
  _contactList(std::make_shared<ContactListModel>()),
  _requestList(std::make_shared<ContactListModel>()),
  _chatManager(std::make_shared<ChatListModel>()),
  _presenceHandler(std::make_unique<PresenceHandler>(_client,_contactList,_requestList))
{
  setupUi();
  _client->logger()->setLoggingType(QXmppLogger::StdoutLogging);
  // Setup popup views
  _contactListDelegate = std::make_unique<ContactListDelegate>(_requestListView);
  _requestListView->setModel(_requestList.get());
  _requestListView->setItemDelegate(_contactListDelegate.get());
  _createChatContactView->setModel(_contactList.get());
  _createChatContactView->setItemDelegate(_contactListDelegate.get());
  // QXmppClient
  connect(_client.get(), &QXmppClient::error, _clientErr.get(), &ClientErrHandler::manageClientError);
  connect(_client.get(), &QXmppClient::sslErrors, _clientErr.get(), &ClientErrHandler::manageSslError);
  connect(_client->findExtension<QXmppRosterManager>(), &QXmppRosterManager::subscriptionReceived, _requestList.get(), &ContactListModel::addRequest);
  connect(_client->findExtension<QXmppMessageReceiptManager>(), &QXmppMessageReceiptManager::messageDelivered, _chatManager.get(), &ChatListModel::recvReceipt);
  connect(_client.get(), &QXmppClient::messageReceived, _chatManager.get(), &ChatListModel::recvMsg);
  // Ui::Login
  connect(_login, &LoginUi::sendXmppCreds, this, &HomeUi::initDb);
  connect(_login, &LoginUi::connectBtClicked , this, &HomeUi::setHomeCurrentwidget);
  // Ui::HomePage
  connect(_chatListCont, &ChatListCont::chatButtonClicked, this, &HomeUi::popupCreateChat);
  connect(_contactListCont, &ContactListCont::createContactClicked, this, &HomeUi::manageCreateContact);
  connect(_contactListCont, &ContactListCont::requestListClicked, this, &HomeUi::popupRequestList);
  // Ui::Internal
  connect(_requestList.get(), &ContactListModel::rowsInserted, this, &HomeUi::requestUpdated);
  connect(_requestList.get(), &ContactListModel::rowsRemoved, this, &HomeUi::requestUpdated);
  connect(_requestListView, &ContactListView::lostFocus, this, &HomeUi::hideRequestList);
  connect(_requestListView, &QListView::doubleClicked, this, &HomeUi::showRequestDialogue);
  connect(_createChatContactView, &ContactListView::lostFocus, this, &HomeUi::hideCreateChat);
  connect(_createChatContactView, &QListView::doubleClicked, this, &HomeUi::addChatFromContact);
}

HomeUi::~HomeUi() {
  _client->disconnectFromServer();
}

void HomeUi::setupUi() {
  QPalette palette;
  palette.setColor(QPalette::Window, Style::HomeStyle::HomeUi.backgroundColor);
  this->setPalette(palette);
  this->setAutoFillBackground(true);
  setGeometry(dynamic_cast<QWidget*>(parent())->geometry());
  int r_width=size().width();
  int r_height=size().height();
  _stackedWidget = new QStackedWidget(this);
  _homePage = new QWidget(_stackedWidget);
  _stackedWidget->addWidget(_homePage);
  _login = new LoginUi(_stackedWidget);
  _stackedWidget->addWidget(_login);
  _stackedWidget->setCurrentWidget(_login);
  QVBoxLayout *layout = new QVBoxLayout(this);
  layout->setSpacing(0);
  layout->setContentsMargins(0,0,0,0);
  layout->addWidget(_stackedWidget);
  setLayout(layout);
  _chatListCont = new ChatListCont(_homePage,_chatManager.get());
  _chatListCont->setGeometry(r_width/3.,0.,2.*r_width/3.,r_height);
  _contactListCont = new ContactListCont(_homePage,_contactList.get());
  _contactListCont->setGeometry(0, 0.,9.*r_width/30.,r_height);
  _requestListView = new ContactListView(_homePage);
  _requestListView->setVisible(false);
  _createChatContactView = new ContactListView(_homePage);
  _createChatContactView->setVisible(false);
}

void HomeUi::initDb(QXmppConfiguration const& xmpp_creds) {
  _client->connectToServer(xmpp_creds);
  try{
    _sqlite = std::make_shared<SqliteHandler>(xmpp_creds.jid().toStdString());
  }
  catch (const DiskError &){
    std::exit(1);
  }
  _contactList->setSqlite(_sqlite);
  _contactList->setSender(std::make_unique<XmppSender>(_client));
  _sqlite->extractContacts(_contactList.get());
  emit _contactList->initialized();
  QString my_jid = xmpp_creds.jid().split('/').first();
  _contactList->tryAddContact(my_jid,xmpp_creds.jid().split('@').first(),ContactEnum::Both);
  _chatManager->init(parentWidget(),my_jid,std::make_shared<XmppSender>(_client),_sqlite);
}

void HomeUi::setHomeCurrentwidget() {
  _stackedWidget->setCurrentWidget(_homePage);
}

void HomeUi::manageCreateContact() {
  CreateContactUi * create_cont_b = new CreateContactUi(this);
  create_cont_b->setAttribute(Qt::WA_DeleteOnClose);
  create_cont_b->setCloseOnLost();
  create_cont_b->setVisible(true);
  create_cont_b->setFocus();
  connect(create_cont_b, &CreateContactUi::ContactRequestSent, _contactList.get(), &ContactListModel::sendContactRequest);
}

void HomeUi::requestUpdated(const QModelIndex &parent, int first, int last) {
  Q_UNUSED(parent);
  _contactListCont->setIncRequestB(_requestList->rowCount() > 0);
}

void HomeUi::showRequestDialogue(const QModelIndex &index) {
  QMessageBox::StandardButton reply;
  reply = QMessageBox::question(this, "Plz make a real ui", "Accept subscription",
                                QMessageBox::Yes|QMessageBox::No);
  try{
    if (reply == QMessageBox::Yes) {
      _presenceHandler->handleRequest(index,ContactEnum::Accept);
    } else {
      _presenceHandler->handleRequest(index,ContactEnum::Deny);
    }
  }
  catch (const ArgNotInEnum &) {
    logger.log("[HomeUi::showRequestDialogue] requestAction not recognized",LogLevel::Warning);
  }
}

void HomeUi::popupRequestList(const QRect &basePos) {
  int r_width=size().width();
  int r_height=size().height();
  QRect target_pos(r_width*0.25,r_height*0.25,r_width*0.5,r_height*0.5);
  _requestListView->setGeometry(basePos.translated(_contactListCont->geometry().topLeft()));
  _requestListView->setVisible(true);
  _requestListView->setFocus();
  QPropertyAnimation *openlist_animation = new QPropertyAnimation{_requestListView,"geometry"};
  openlist_animation->setDuration(250); // ms
  openlist_animation->setEndValue(target_pos);
  openlist_animation->start(QAbstractAnimation::DeleteWhenStopped);
}

void HomeUi::hideRequestList() {
  _requestListView->setVisible(false);
}

void HomeUi::popupCreateChat(const QRect &basePos) {
  int r_width=size().width();
  int r_height=size().height();
  QRect target_pos(r_width*0.25,r_height*0.25,r_width*0.5,r_height*0.5);
  _createChatContactView->setGeometry(basePos.translated(_chatListCont->geometry().topLeft()));
  _createChatContactView->setVisible(true);
  _createChatContactView->setFocus();
  QPropertyAnimation *openlist_animation = new QPropertyAnimation{_createChatContactView,"geometry"};
  openlist_animation->setDuration(250); // ms
  openlist_animation->setEndValue(target_pos);
  openlist_animation->start(QAbstractAnimation::DeleteWhenStopped);
}

void HomeUi::hideCreateChat() {
  _createChatContactView->setVisible(false);
}

void HomeUi::addChatFromContact(const QModelIndex &index) {
  _chatManager->tryAddChat(index.data(ContactEnum::jid).value<QString>());
}

void HomeUi::resizeEvent(QResizeEvent *event) {
  int r_width=size().width();
  int r_height=size().height();
  _chatListCont->setGeometry(r_width/3.,0.,2.*r_width/3.,r_height);
  _contactListCont->setGeometry(0, 0.,9.*r_width/30.,r_height);
  QWidget::resizeEvent(event);
}

// When is this used? I see no difference removing it
void HomeUi::paintEvent(QPaintEvent *) {
//  QStyleOption opt;
//  opt.initFrom(this);
//  QPainter p(this);
//  style()->drawPrimitive(QStyle::PE_Widget, &opt, &p, this);
}
