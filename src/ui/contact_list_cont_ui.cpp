#include "contact_list_cont_ui.hpp"
#include "../styles/global_style.hpp"

ContactListCont::ContactListCont(QWidget *parent,ContactListModel *contactModel) :
  QFrame(parent) {
  setupUi();
  _contactListDelegate = std::make_unique<ContactListDelegate>(_contactListView);
  _contactListView->setModel(contactModel);
  _contactListView->setItemDelegate(_contactListDelegate.get());
  connect(_createContactB, &QPushButton::clicked, this, [=](){emit createContactClicked();});
  connect(_incRequestB, &QPushButton::clicked, this, [=](){emit requestListClicked(this->_incRequestB->geometry());});
}

ContactListCont::~ContactListCont() {;}

void ContactListCont::setIncRequestB(bool visible) {
  _incRequestB->setVisible(visible);
}

void ContactListCont::setupUi() {
  QPalette palette;
  palette.setColor(QPalette::Window,Style::HomeStyle::contactListCont.CS.backgroundColor);
  this->setPalette(palette);
  this->setAutoFillBackground(true);
  _contactListView = new ContactListView(this);
  _contactListView->setSpacing(10);
  _contactListView->setRadius(Style::HomeStyle::contactListCont.SS.borderRadius);
  QPalette createBtPalette;
  createBtPalette.setColor(QPalette::Button, Style::HomeStyle::addContact.CS.backgroundColor);
  _createContactB = new RoundButton("+",this);
  _createContactB->setPalette(createBtPalette);
  _createContactB->setRadius(Style::HomeStyle::addContact.SS.borderRadius);
  _incRequestB = new RoundButton("",this);
  _incRequestB->setVisible(false);
}

void ContactListCont::resizeEvent(QResizeEvent *event) {
  int r_width = size().width();
  int r_height = size().height();
  _contactListView->setGeometry(r_width/60.,r_height/60.,58.*r_width/60., 58.*r_height/60.);
  _createContactB->setGeometry(98.*r_width/120., 37.*r_height/40.,6.*r_width/40.,r_height/20.);
  _incRequestB->setGeometry(2.*r_width/53., 35.*r_height/40.,r_width/7.,r_height/20.);
  QFrame::resizeEvent(event);
}
