#ifndef RYTCH_CHAT_UI_HPP_INCLUDED
#define RYTCH_CHAT_UI_HPP_INCLUDED

#include <QWidget>
#include <QPlainTextEdit>

#include "../models/chat_model.hpp"
#include "../delegates/chat_delegate.hpp"
#include "../views/chat_view.hpp"
#include "../styles/returnb_ui.hpp"
#include "../styles/roundbutton.hpp"

class ChatUi : public QWidget {

  Q_OBJECT

  public:
    ChatUi(QWidget *parent=nullptr, ChatModel *chatModel=nullptr);
    ChatUi(const ChatUi &) = delete;
    ~ChatUi();

  signals:
    void sendRequest(QStringView);

  private slots:
    void sendB();
    void returnB();
    void onTextChanged();

  private:
    void setupUi();
    void resizeEvent(QResizeEvent *event) override;
    void showEvent(QShowEvent *event) override;
    bool eventFilter(QObject *watched, QEvent* event) override;
    std::unique_ptr<ChatDelegate> _msgDelegate;
    int _prevNblines = 0;
    // Widgets
    QFrame * _chatCont;
    ChatView *_msgView;
    ReturnButtonUi * _returnB;
    RoundButton *_sendB;
    QPlainTextEdit *_msgEdit;
};

#endif
