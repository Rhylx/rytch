#ifndef RYTCH_MAINWINDOW_HPP
#define RYTCH_MAINWINDOW_HPP

#include <QMainWindow>
#include "home_ui.hpp"

class MainWindow : public QMainWindow {

  Q_OBJECT

  public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

  private:
    void setupUi();
    void resizeEvent(QResizeEvent *event);
    QWidget *_centralWidget;
    HomeUi* _homeUi;
};

#endif
