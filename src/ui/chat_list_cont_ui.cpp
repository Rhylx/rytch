#include "chat_list_cont_ui.hpp"
#include "../styles/global_style.hpp"

ChatListCont::ChatListCont(QWidget *parent,ChatListModel *chatModel) :
  QFrame(parent) {
  setupUi();
  _chatListDelegate = std::make_unique<ChatListDelegate>(_chatListView);
  _chatListView->setModel(chatModel);
  _chatListView->setItemDelegate(_chatListDelegate.get());
  connect(_createChatB, &QPushButton::clicked, this, [=](){emit chatButtonClicked(this->_createChatB->geometry());});
}

ChatListCont::~ChatListCont() {;}

void ChatListCont::setupUi() {
  QPalette palette;
  palette.setColor(QPalette::Window,Style::HomeStyle::chatListCont.CS.backgroundColor);
  this->setPalette(palette);
  this->setAutoFillBackground(true);
  _chatListView = new ChatListView(this);
  _chatListView->setSpacing(10);
  _chatListView->setRadius(Style::HomeStyle::chatListCont.SS.borderRadius);
  QPalette btPalette;
  btPalette.setColor(QPalette::Button, Style::HomeStyle::addChat.CS.backgroundColor);
  _createChatB = new RoundButton("+",this);
  _createChatB->setPalette(btPalette);
  _createChatB->setRadius(Style::HomeStyle::addChat.SS.borderRadius);
}

void ChatListCont::resizeEvent(QResizeEvent *event) {
  int r_width = size().width();
  int r_height = size().height();
  _chatListView->setGeometry(r_width/60.,r_height/60.,58.*r_width/60., 58.*r_height/60.);
  _createChatB->setGeometry(98.*r_width/120., 37.*r_height/40.,6.*r_width/40.,r_height/20.);
  QFrame::resizeEvent(event);
}

