#ifndef CONTACTLISTCONT_UI_HPP
#define CONTACTLISTCONT_UI_HPP

#include <QFrame>

#include "../styles/roundbutton.hpp"
#include "../views/contact_list_view.hpp"
#include "../delegates/contact_list_delegate.hpp"
#include "../models/contact_list_model.hpp"

class ContactListCont : public QFrame {

  Q_OBJECT

  public:
    ContactListCont(QWidget *parent,ContactListModel *contactModel);
    ~ContactListCont();

    void setIncRequestB(bool visible);

  signals:
    void createContactClicked();
    void requestListClicked(const QRect &);

  private:
    void setupUi();
    void resizeEvent(QResizeEvent *event) override;
    RoundButton *_createContactB,*_incRequestB;
    ContactListView *_contactListView;
    std::unique_ptr<ContactListDelegate> _contactListDelegate;
};

#endif
