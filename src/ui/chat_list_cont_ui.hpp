#ifndef CHATLISTCONT_UI_HPP
#define CHATLISTCONT_UI_HPP

#include <QFrame>

#include "../views/chat_list_view.hpp"
#include "../delegates/chat_list_delegate.hpp"
#include "../models/chat_list_model.hpp"
#include "../styles/roundbutton.hpp"

class ChatListCont : public QFrame {

  Q_OBJECT

  public:
    ChatListCont(QWidget *parent,ChatListModel *chatModel);
    ~ChatListCont();

  signals:
    void chatButtonClicked(const QRect &);

  private:
    void setupUi();
    void resizeEvent(QResizeEvent *event) override;
    RoundButton *_createChatB;
    ChatListView *_chatListView;
    std::unique_ptr<ChatListDelegate> _chatListDelegate;
};

#endif
