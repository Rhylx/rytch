#include "chat_list_delegate.hpp"

#include "../styles/global_style.hpp"

#include "../structures/chat_struct.hpp"

#include <QPainter>
#include <QEvent>
#include <QApplication>
#include <QPainterPath>


void ChatListDelegate::paint(QPainter *painter, const QStyleOptionViewItem &option, const QModelIndex &index) const {
  Q_ASSERT(index.isValid());
  QStyleOptionViewItem opt = option;
  initStyleOption(&opt, index);
  painter->save();
  // Draw background
  QRect buttonRect = option.rect.adjusted(1, 1, -1, -1);
  QPainterPath borderPath;
  borderPath.addRoundedRect(buttonRect,Style::ChatListStyle::item.SS.borderRadius,Style::ChatListStyle::item.SS.borderRadius);
  painter->fillPath(borderPath, Style::ChatListStyle::item.CS.backgroundColor);
  // Draw text
  painter->setPen(Style::ChatListStyle::item.CS.textColor);
  painter->setFont(opt.font);
  painter->drawText(opt.rect, Qt::AlignCenter,index.data(label).value<QString>());
  painter->restore();
}


QSize ChatListDelegate::sizeHint(const QStyleOptionViewItem &option, const QModelIndex &index) const {
  QString text = index.data(label).toString();
  QRectF r = option.rect;
  QFontMetricsF fm(option.font);
  int m_currentItemHeight = fm.boundingRect(r, Qt::AlignJustify | Qt::AlignVCenter | Qt::TextWordWrap, text).height();
  QSize size = QStyledItemDelegate::sizeHint(option, index);
  size.setHeight(m_currentItemHeight);
  size.setWidth(size.width());
  return size;
}

bool ChatListDelegate::editorEvent(QEvent *event, QAbstractItemModel *model, const QStyleOptionViewItem &option, const QModelIndex &index) {
  Q_UNUSED(model);
  Q_UNUSED(option);
  switch(event->type()) {
  case (QEvent::MouseButtonDblClick) :
    index.data(chatRole::uiP).value<QWidget*>()->show();
    return true; // Event handled
  default:
    return false;
 }
}


