#ifndef CHAT_DELEGATE_HPP_INCLUDED
#define CHAT_DELEGATE_HPP_INCLUDED

#include <QStyledItemDelegate>

class ChatDelegate : public QStyledItemDelegate {
  public:
    ChatDelegate(QObject *parent = nullptr) : QStyledItemDelegate(parent) {;}
    ~ChatDelegate() {;}
    void paint(QPainter *painter, const QStyleOptionViewItem &option, const QModelIndex &index) const override;
    QSize sizeHint(const QStyleOptionViewItem &option, const QModelIndex &index) const override;
};

#endif
