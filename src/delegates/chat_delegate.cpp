#include "chat_delegate.hpp"

#include <QPainter>

#include "../structures/message_struct.hpp"

// TODO REMOVEME
#include <iostream>

namespace ChatDelegateImpl {
  int computeAppropriateFlag(int const maxwidth, const QRect & rect, QFontMetrics const &fontMetrics, const QString& msgbody) {
    QRect textRect = fontMetrics.boundingRect(rect,Qt::TextWordWrap,msgbody);
    if (maxwidth < textRect.width()) {
      return Qt::TextWrapAnywhere;
    } else {
      return Qt::TextWordWrap;
    }
  }
}

void ChatDelegate::paint(QPainter *painter, const QStyleOptionViewItem &option, const QModelIndex &index) const {
  QString msgbody = index.data(msgRole::body).value<QString>();
  bool isfromMe = index.data(msgRole::fromSelf).toBool();
  QColor bubbleColor = isfromMe ? QColor(200,200,255) : QColor(200,255,200);
  // Compute the appropriate flag
  int padding = 10;
  int borderRadius = 10;
  QFontMetrics fontMetrics(option.font);
  QRect itemRect = option.rect;
  int maxwidth = 1.*itemRect.width();
  int flag = ChatDelegateImpl::computeAppropriateFlag(maxwidth,itemRect.adjusted(padding, padding, -padding, -padding),fontMetrics,msgbody);
  flag |= isfromMe? Qt::AlignLeft : Qt::AlignRight;

  QRect textRect = fontMetrics.boundingRect(itemRect.adjusted(padding, padding, -padding, -padding), flag, msgbody);
  if (isfromMe) {
    textRect.moveRight(itemRect.right() - padding);
  }
  else {
    textRect.moveLeft(itemRect.left() + padding);
  }
  QRect bubbleRect = textRect.adjusted(-padding, -padding, padding, padding);
  painter->save();
  painter->setRenderHint(QPainter::Antialiasing,true);
  painter->setPen(Qt::NoPen);
  painter->setBrush(bubbleColor);
  painter->drawRoundedRect(bubbleRect, borderRadius, borderRadius);
  painter->setFont(option.font);
  painter->setPen(Qt::black);
  painter->drawText(textRect, flag, msgbody);
  painter->restore();
}

QSize ChatDelegate::sizeHint(const QStyleOptionViewItem &option, const QModelIndex &index) const {
  QString msgbody = index.data(msgRole::body).value<QString>();
  int padding = 10;
  QRect itemRect = option.rect;
  QFontMetrics fontMetrics(option.font);
  int maxwidth = 1.*itemRect.width();
  int flag = ChatDelegateImpl::computeAppropriateFlag(maxwidth,itemRect.adjusted(padding, padding, -padding, -padding),fontMetrics,msgbody);
  QRect textRect = fontMetrics.boundingRect(itemRect.adjusted(padding, padding, -padding, -padding),flag,msgbody);
  return QSize(100,textRect.height()+3*padding);
}
