#ifndef CHAT_LIST_DELEGATE_HPP_INCLUDED
#define CHAT_LIST_DELEGATE_HPP_INCLUDED

#include <QStyledItemDelegate>

class ChatListDelegate : public QStyledItemDelegate {

  Q_OBJECT

  public:
    ChatListDelegate(QObject *parent = nullptr) : QStyledItemDelegate(parent) {;}
    ~ChatListDelegate() {;}
    void paint(QPainter *painter, const QStyleOptionViewItem &option, const QModelIndex &index) const override;
    QSize sizeHint(const QStyleOptionViewItem &option, const QModelIndex &index) const override;

  protected:
    bool editorEvent(QEvent *event, QAbstractItemModel *model, const QStyleOptionViewItem &option, const QModelIndex &index) override;
};

#endif
