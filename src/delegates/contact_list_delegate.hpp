#ifndef CONTACT_LIST_DELEGATE_HPP_INCLUDED
#define CONTACT_LIST_DELEGATE_HPP_INCLUDED

#include <QStyledItemDelegate>

class ContactListDelegate : public QStyledItemDelegate {
  public:
    ContactListDelegate(QObject *parent = nullptr) : QStyledItemDelegate(parent) {;}
    ~ContactListDelegate() {;}

    void paint(QPainter *painter, const QStyleOptionViewItem &option, const QModelIndex &index) const override;
    QSize sizeHint(const QStyleOptionViewItem &option, const QModelIndex &index) const override;

};

#endif
