#include "contact_list_delegate.hpp"

#include "../structures/contact_struct.hpp"
#include "../styles/global_style.hpp"

#include <QPainter>
#include <QEvent>
#include <QApplication>
#include <QPainterPath>

// TODO REMOVEME
#include <iostream>

using namespace ContactEnum;

void ContactListDelegate::paint(QPainter *painter, const QStyleOptionViewItem &option, const QModelIndex &index) const {
  Q_ASSERT(index.isValid());
  QStyleOptionViewItem opt = option;
  initStyleOption(&opt, index);
  int c_width = option.rect.width();
  int c_height = option.rect.height();
  int c_x = option.rect.x();
  int c_y = option.rect.y();
  painter->save();
  // Draw background
  QRect buttonRect = option.rect.adjusted(1, 1, -1, -1);
  QPainterPath borderPath;
  borderPath.addRoundedRect(buttonRect,Style::ContactListStyle::item.SS.borderRadius,Style::ContactListStyle::item.SS.borderRadius);
  painter->fillPath(borderPath, Style::ContactListStyle::item.CS.backgroundColor);
  // Draw name
  painter->setPen(Style::ContactListStyle::item.CS.textColor);
  painter->setFont(opt.font);
  painter->drawText(opt.rect, Qt::AlignCenter,index.data(name).value<QString>());
  // Draw availability
  QColor statusColor;
  switch (index.data(status).value<AvailableStatus>()) {
    case(AvailableStatus::Online):
      statusColor = Style::ContactListStyle::status.onlineColor;
      break;
    case(AvailableStatus::Offline):
      statusColor = Style::ContactListStyle::status.offlineColor;
      break;
    case(AvailableStatus::Away):
      statusColor = Style::ContactListStyle::status.awayColor;
      break;
    case(AvailableStatus::XA):
      statusColor = Style::ContactListStyle::status.xaColor;
      break;
    case(AvailableStatus::DND):
      statusColor = Style::ContactListStyle::status.dndColor;
      break;
    case(AvailableStatus::Chat):
      statusColor = Style::ContactListStyle::status.chatColor;
      break;
    case(AvailableStatus::Unknown):
    default:
      statusColor = Style::ContactListStyle::status.unknownColor;
      break;
  }
  painter->setRenderHint(QPainter::Antialiasing, true);
  painter->setBrush(QBrush{statusColor});
  painter->drawEllipse(c_x+c_width*0.9,c_y+c_height*0.3,c_height*0.4,c_height*0.4);
  // Draw Subscription status
  painter->setBrush(QBrush{0x000000u});
  if (index.data(inroster).value<SubscriptionStatus>() & SubscriptionStatus::From) {
    const QPointF points[4] = {
            QPointF(c_x+c_width*0.10,c_y),
            QPointF(c_x+c_width*0.12,c_y),
            QPointF(c_x+c_width*0.11,c_y+c_height),
            QPointF(c_x+c_width*0.09,c_y+c_height)};
    painter->drawPolygon(points,4);
  }
  if (index.data(inroster).value<SubscriptionStatus>() & SubscriptionStatus::To) {
    const QPointF points[4] = {
            QPointF(c_x+c_width*0.14,c_y),
            QPointF(c_x+c_width*0.16,c_y),
            QPointF(c_x+c_width*0.15,c_y+c_height),
            QPointF(c_x+c_width*0.13,c_y+c_height)};
    painter->drawPolygon(points,4);
  }
  painter->restore();
}

QSize ContactListDelegate::sizeHint(const QStyleOptionViewItem &option, const QModelIndex &index) const {
  QString text = index.data(name).toString();
  QRectF r = option.rect;
  QFontMetricsF fm(option.font);
  int m_currentItemHeight = fm.boundingRect(r, Qt::AlignJustify | Qt::AlignVCenter | Qt::TextWordWrap, text).height();
  QSize size = QStyledItemDelegate::sizeHint(option, index);
  size.setHeight(m_currentItemHeight);
  size.setWidth(size.width() - 10);
  return size;
}

