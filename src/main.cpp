#include <QApplication>
#include <QFile>

#include "ui/mainwindow.hpp"
#include "styles/global_style.hpp"
#include "core/logger.hpp"

int main(int argc, char *argv[]) {
  QApplication app(argc,argv);
  Style::setDefault();
logger.setLogLevel(9);
logger.dateStamp();
logger.log("Start",LogLevel::Info);
  MainWindow mwindow;
  mwindow.show();
  return app.exec();
}
