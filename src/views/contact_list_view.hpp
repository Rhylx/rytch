#ifndef CONTACT_VIEW_HPP
#define CONTACT_VIEW_HPP

#include "../styles/roundlistview.hpp"


class ContactListView : public RoundListView {

  Q_OBJECT

  public:
    ContactListView(QWidget * parent = nullptr);

  signals:
    void lostFocus();

  protected:
    void focusOutEvent(QFocusEvent *event) override;

  private:
    QStyleOptionViewItem viewOptions() const;
};

#endif
