#ifndef CHAT_VIEW_HPP
#define CHAT_VIEW_HPP


#include <QAbstractItemView>
#include <QListView>


class ChatView : public QListView {
  public:
    ChatView(QWidget * parent = nullptr);

  private:
    QStyleOptionViewItem viewOptions() const;
};

#endif
