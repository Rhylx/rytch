#include "chat_view.hpp"
#include "../styles/global_style.hpp"


ChatView::ChatView(QWidget *parent) : QListView(parent) {
  QPalette palette = this->palette();
  palette.setColor(QPalette::Base,Style::ChatStyle::ChatUi.backgroundColor);
  this->setPalette(palette);
  setResizeMode(QListView::Adjust);
}


QStyleOptionViewItem ChatView::viewOptions() const {
  QStyleOptionViewItem option;
  option.init(this);
  option.state &= ~QStyle::State_MouseOver;
  // On mac the focus appearance follows window activation
  // not widget activation
  if (!hasFocus())
    option.state &= ~QStyle::State_Active;
  option.state &= ~QStyle::State_HasFocus;
  if (this->iconSize().isValid()) {
    option.decorationSize = this->iconSize();
  } else {
    int pm = style()->pixelMetric(QStyle::PM_SmallIconSize, nullptr, this);
    option.decorationSize = QSize(pm, pm);
  }
  option.decorationPosition = QStyleOptionViewItem::Left;
  option.decorationAlignment = Qt::AlignLeft;
  option.displayAlignment = Qt::AlignLeft|Qt::AlignVCenter;
  option.textElideMode = this->textElideMode();
  option.showDecorationSelected = style()->styleHint(QStyle::SH_ItemView_ShowDecorationSelected, nullptr, this);
  option.locale = locale();
  option.locale.setNumberOptions(QLocale::OmitGroupSeparator);
  option.font = QFont("Arial", 15);
  option.palette.setColor(QPalette::Text,Qt::black);
  option.widget = this;
  return option;
}
