#include "chat_list_view.hpp"
#include "../styles/global_style.hpp"

ChatListView::ChatListView(QWidget *parent) : RoundListView(parent) {
  QPalette palette = this->palette();
  palette.setColor(QPalette::Window,Style::HomeStyle::chatList.CS.backgroundColor);
  this->setPalette(palette);
}

// Is it used ? I do not think we use the style functions
QStyleOptionViewItem ChatListView::viewOptions() const {
  QStyleOptionViewItem option;
  option.init(this);
  option.state &= ~QStyle::State_MouseOver;
  // On mac the focus appearance follows window activation
  // not widget activation
  if (!hasFocus())
      option.state &= ~QStyle::State_Active;
  option.state &= ~QStyle::State_HasFocus;
  if (this->iconSize().isValid()) {
      option.decorationSize = this->iconSize();
  } else {
    int pm = style()->pixelMetric(QStyle::PM_SmallIconSize, nullptr, this);
    option.decorationSize = QSize(pm, pm);
  }
  option.decorationPosition = QStyleOptionViewItem::Left;
  option.decorationAlignment = Qt::AlignLeft;
  option.displayAlignment = Qt::AlignLeft|Qt::AlignVCenter;
  option.textElideMode = this->textElideMode();
  option.rect = QRect();
  option.showDecorationSelected = style()->styleHint(QStyle::SH_ItemView_ShowDecorationSelected, nullptr, this);
  option.locale = locale();
  option.locale.setNumberOptions(QLocale::OmitGroupSeparator);
  option.font = QFont("Arial", 30);
  option.palette.setColor(QPalette::Text,Qt::black);
  option.widget = this;
  return option;
}
