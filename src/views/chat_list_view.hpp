#ifndef CHAT_LIST_VIEW_HPP
#define CHAT_LIST_VIEW_HPP

#include "../styles/roundlistview.hpp"

class ChatListView : public RoundListView {
  public:
    ChatListView(QWidget * parent = nullptr);

  private:
    QStyleOptionViewItem viewOptions() const;
};

#endif
