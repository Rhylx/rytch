#ifndef MESSAGES_MODEL_HPP_INCLUDED
#define MESSAGES_MODEL_HPP_INCLUDED

#include <QAbstractListModel>
#include <QXmppMessage.h>

#include "../structures/chat_list_struct.hpp"
#include "../structures/message_struct.hpp"

class ChatModel : public QAbstractListModel {
  public:
    ChatModel(ChatListStruct chat_m_struct);
    ChatModel(const ChatModel &) = delete;
    ~ChatModel() {;}
    void insertSentMsg(QXmppMessage & inc_msg);
    void insertRecvMsg(const QXmppMessage & sent_msg);
    void addMsgToModel(const MessageStruct & recv_msg);
    int rowCount(const QModelIndex &parent = QModelIndex()) const override{
        return _msgVect.size();
    }
    int id() const {
      return _chatListStruct.chatId;
    }
    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;

  public slots:
    void sendMsg(QStringView);

  private:
    bool isValid(const QXmppMessage & recv_msg);
    int handleErrorMsg(const QXmppMessage &);

  private:
    ChatListStruct _chatListStruct;
    std::vector<MessageStruct> _msgVect;
};

#endif
