#ifndef CHAT_LIST_MODEL_HPP_INCLUDED
#define CHAT_LIST_MODEL_HPP_INCLUDED

#include <QAbstractListModel>

#include "../structures/chat_struct.hpp"
#include "../structures/contact_struct.hpp"
#include "../core/sqlite_handler.hpp"

class SqliteHandler;
class XmppSender;

class ChatListModel : public QAbstractListModel {

  Q_OBJECT

  public:
    // Override
    int rowCount(const QModelIndex &parent = QModelIndex()) const override;
    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;
    // No constructor because it needs an sqlite_handler to be initialized
    void init(QWidget *parent, QString const & jid, std::shared_ptr<XmppSender> sender, std::shared_ptr<SqliteHandler> sqlite);
    //
    int tryAddChat(QString const & jid);

    friend SqliteForwarded;

  public slots:
    void recvMsg(const QXmppMessage &);
    void recvReceipt(const QString & , const QString &);

  private:
    void addChatToModel(ChatStruct &data);

  private:
    QWidget * _chatsParent;
    QString _myJid;
    std::shared_ptr<XmppSender> _sender;
    std::shared_ptr<SqliteHandler> _sqlite;
    std::vector<ChatStruct> _chatVect;
};

#endif
