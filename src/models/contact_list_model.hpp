#ifndef CONTACT_LIST_MODEL_HPP_INCLUDED
#define CONTACT_LIST_MODEL_HPP_INCLUDED

#include <QAbstractListModel>

#include "../core/xmpp_sender.hpp"
#include "../structures/contact_struct.hpp"
#include "../core/sqlite_handler.hpp"

class ContactListModel: public QAbstractListModel
{

  Q_OBJECT

  public:
    // Override
    int rowCount(const QModelIndex &parent = QModelIndex()) const override;
    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;
    // Global state setter
    void setSqlite(std::shared_ptr<SqliteHandler> p_sqlite) {_sqlite = p_sqlite;}
    void setSender(std::unique_ptr<XmppSender> p_sender) {_sender = std::move(p_sender);}
    // Data lookup
    size_t getRow(QStringView jid) const;
    void getContactBareJids(QStringList & jids) const;
    // Update state
    void setAvailableStatus(QStringView jid,ContactEnum::AvailableStatus status);
    void setSubscriptionStatus(QString jid,ContactEnum::SubscriptionStatus status);
    void tryAddContact(const QString &new_jid, const QString &new_name, ContactEnum::SubscriptionStatus inroster= ContactEnum::SubscriptionStatus::None);
    void removeEntry(size_t row);

    friend SqliteForwarded;

  public slots:
    void sendContactRequest(const QString &jid);
    void addRequest(const QString &bareJid);

  signals:
    void initialized();

  private:
    void addContactToModel(ContactStruct &data);

  private:
    std::vector<ContactStruct> _contactVect;
    std::shared_ptr<SqliteHandler> _sqlite;
    std::unique_ptr<XmppSender> _sender;
};

#endif
