#include "contact_list_model.hpp"

#include <algorithm>

#include "../core/logger.hpp"
#include <sstream>
#include "../structures/error_types.hpp"
#include "../core/sqlite_handler.hpp"

using namespace ContactEnum;

int ContactListModel::rowCount(const QModelIndex &parent) const {
    Q_UNUSED(parent);
    return _contactVect.size();
}

QVariant ContactListModel::data(const QModelIndex &index, int role) const {
  switch(role) {
    case contactRole::name :
      return QVariant::fromValue(_contactVect.at(index.row()).name);
    case contactRole::jid :
      return QVariant::fromValue(_contactVect.at(index.row()).jid);
    case contactRole::inroster :
      return QVariant::fromValue(_contactVect.at(index.row()).inroster);
    case contactRole::status :
      return QVariant::fromValue(_contactVect.at(index.row()).status);
    default:
      return QVariant();
  }
}

size_t ContactListModel::getRow(QStringView jid) const {
  auto searchjid = [jid](const ContactStruct &it){return it.jid == jid;};
  auto result = std::find_if(_contactVect.cbegin(),_contactVect.cend(),searchjid);
  if (result != _contactVect.cend()) {
    return result - _contactVect.cbegin();
  } else {
    throw Missing("Contact_handler: Contact id not found");
  }
}

void ContactListModel::getContactBareJids(QStringList & jids) const {
  for (auto const & contact : _contactVect) {
    jids.push_back(contact.jid);
  }
}

void ContactListModel::setAvailableStatus(QStringView jid,AvailableStatus status) {
  int row;
  try{
    row = getRow(jid);
  }
  catch (const Missing &) {
    std::stringstream stream;
    stream << "[ContactListModel::setAvailableStatus] Missing jid " << jid.toString().toStdString();
    logger.log(stream.str(),LogLevel::Error);
    return;
  }
  _contactVect[row].status = status;
  QModelIndex c_index = index(row,0);
  emit dataChanged(c_index,c_index,QVector<int>(contactRole::status));
}

void ContactListModel::setSubscriptionStatus(QString jid,SubscriptionStatus status) {
  // Try to add to contact if not already present
  int row;
  try {
    row = getRow(jid);
  } catch (const Missing &) {
    tryAddContact(jid,jid.split('@').first(),status);
    return;
  }
  _contactVect[row].inroster = status;
  try {
  _sqlite->updateSubscriptionStatus(_contactVect[row].jid,status);
  } catch (const DiskError &) {
    logger.log("[ContactListModel::setSubscriptionStatus] Unable to update the database on disk",LogLevel::Error);
    std::exit(1);
  }
  QModelIndex c_index = index(row,0);
  emit dataChanged(c_index,c_index,QVector<int>(contactRole::inroster));
}

void ContactListModel::tryAddContact(const QString &new_jid, const QString &new_name, SubscriptionStatus inroster) {
  auto searchjid = [new_jid](const ContactStruct &it){return it.jid == new_jid;};
  auto result = std::find_if(_contactVect.cbegin(),_contactVect.cend(),searchjid);
  if (result == _contactVect.cend()) {
    ContactStruct data;
    data.jid=new_jid;
    data.name=new_name;
    data.inroster=static_cast<SubscriptionStatus>(inroster);
    try {
      _sqlite->insertContact(data);
    } catch (const DiskError &) {
      logger.log("[ContactListModel::tryAddContact] Unable to update the database on disk",LogLevel::Error);
      std::exit(1);
    }
    addContactToModel(data);
  } 
}

void ContactListModel::removeEntry(size_t row) {
  beginRemoveRows(QModelIndex(),row,row+1);
  _contactVect.erase(_contactVect.cbegin()+row);
  endRemoveRows();
}

void ContactListModel::sendContactRequest(const QString &jid) {
  _sender->sendContactRequest(jid);
}

void ContactListModel::addRequest(const QString &bareJid) {
  {
    std::stringstream stream;
    stream << "[ContactListModel::addRequest] Request received from: "<<bareJid.toStdString();
    logger.log(stream.str(),LogLevel::Spam);
  }
  ContactStruct data;
  data.jid = bareJid;
  data.name = bareJid;
  beginInsertRows(QModelIndex(),_contactVect.size(),_contactVect.size());
  _contactVect.emplace_back(data);
  endInsertRows();
}

void ContactListModel::addContactToModel(ContactStruct &data) {
  QString jid = data.jid;
  auto searchid = [jid](const ContactStruct &it){return it.jid == jid;};
  auto result = std::find_if(_contactVect.cbegin(),_contactVect.cend(),searchid);
  if (result == _contactVect.cend()) {
    beginInsertRows(QModelIndex(),_contactVect.size(),_contactVect.size());
    _contactVect.emplace_back(data);
    endInsertRows();
  } else {
    throw std::runtime_error("Contact_handler: Shouldn't happend");
  }
}

void SqliteForwarded::createContactFwd(ContactListModel *contact, ContactStruct &data) {
  contact->addContactToModel(data);
}
