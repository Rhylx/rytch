#include "chat_list_model.hpp"
#include "../structures/error_types.hpp"
#include "../core/logger.hpp"

#include <algorithm>

#include "../ui/chat_ui.hpp"
#include "../core/sqlite_handler.hpp"
#include "../core/xmpp_sender.hpp"

// For Windows
#include <string>
#include <algorithm> // std::find_if

int ChatListModel::rowCount(const QModelIndex &parent) const {
  Q_UNUSED(parent);
  return _chatVect.size();
}

QVariant ChatListModel::data(const QModelIndex &index, int role) const {
  switch(role) {
    case chatRole::label :
      return QVariant::fromValue(_chatVect.at(index.row()).chatLabel);
    case chatRole::uiP :
      return QVariant::fromValue(_chatVect.at(index.row()).chatUi.get());
    default:
      return QVariant();
  }
}

void ChatListModel::init(QWidget * parent, QString const & jid, std::shared_ptr<XmppSender> sender, std::shared_ptr<SqliteHandler> sqlite) {
  _chatsParent = parent;
  _myJid = jid;
  _sender = sender;
  _sqlite = sqlite;
  _sqlite->extractChats(this);
}

int ChatListModel::tryAddChat(QString const & jid) {
  auto searchjid = [jid](const ChatStruct &it){return it.recpJid == jid;};
  auto result = std::find_if(_chatVect.cbegin(),_chatVect.cend(),searchjid);
  if (result == _chatVect.cend()) {
    ChatStruct data;
    data.chatLabel = jid.split('@').first();
    data.recpJid = jid;
    int newuid;
    try{
      newuid = _sqlite->insertChat(data);
    } catch (const DiskError &) {
      logger.log("[ChatListModel]::tryAddChat] Unable to write chat to disk.",LogLevel::Error);
      std::exit(1);
    }
    data.chatId = newuid;
    addChatToModel(data);
    return newuid;
  } else {
    return result->chatId;
  }
}

void ChatListModel::recvMsg(const QXmppMessage & inc_msg) {
  QString dest = inc_msg.from().split('/').first();
  auto result = std::find_if(_chatVect.begin(),_chatVect.end(),[&dest](const ChatStruct &arg) {return (arg.recpJid.compare(dest,Qt::CaseInsensitive) == 0);});
  if (result != _chatVect.end()) {
    result->chatM->insertRecvMsg(inc_msg);
  }
}

void ChatListModel::recvReceipt(const QString & jid, const QString & msg_id) {
  int64_t recv_date=QDateTime::currentDateTime().toSecsSinceEpoch();
  _sqlite->updateMsgDate(msg_id, jid, recv_date);

}

void ChatListModel::addChatToModel(ChatStruct &data) {
  int id = data.chatId;
  auto searchid = [id](const ChatStruct &it){return it.chatId == id;};
  auto result = std::find_if(_chatVect.cbegin(),_chatVect.cend(),searchid);
  if (result == _chatVect.cend()) {
    // Fill the Model struct
    ChatListStruct chat_m_data;
    chat_m_data.chatId = id;
    chat_m_data.myJid = _myJid;
    chat_m_data.recpJid = data.recpJid;
    chat_m_data.sender = _sender;
    chat_m_data.sqlite = _sqlite;
    // Create the Chat ui and Chat model
    data.chatM = std::make_shared<ChatModel>(chat_m_data);
    data.chatUi = std::make_shared<ChatUi>(_chatsParent,data.chatM.get());
    connect(data.chatUi.get(),&ChatUi::sendRequest,data.chatM.get(), &ChatModel::sendMsg);
    beginInsertRows(QModelIndex(),_chatVect.size(),_chatVect.size());
    _chatVect.emplace_back(data);
    endInsertRows();
  } else {
    throw std::runtime_error("Chat_list: Non unique uid");
  }
}

void SqliteForwarded::CreateChatFwd(ChatListModel* chat,ChatStruct & data) {
  chat->addChatToModel(data);
}
