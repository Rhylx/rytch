#include "chat_model.hpp"
#include "../structures/error_types.hpp"

#include "../core/sqlite_handler.hpp"

#include "../core/logger.hpp"
#include <sstream>

ChatModel::ChatModel(ChatListStruct chat_m_struct) : _chatListStruct(chat_m_struct) {
    _chatListStruct.sqlite->extractMessages(this);
}

QVariant ChatModel::data(const QModelIndex &index, int role) const {
  switch(role) {
    case msgRole::body :
      return QVariant::fromValue(_msgVect.at(index.row()).body);
    case msgRole::fromSelf :
      return QVariant::fromValue(_msgVect.at(index.row()).senderJid==_chatListStruct.myJid);
    default:
      return QVariant();
  }
}

void ChatModel::insertSentMsg(QXmppMessage & sent_msg) {
  MessageStruct data;
  data.body = sent_msg.body();
  data.senderJid = _chatListStruct.myJid;
  data.recpJid = _chatListStruct.recpJid;
  data.chatId = _chatListStruct.chatId;
  data.dateSent = QDateTime::currentDateTime().toSecsSinceEpoch();
  data.dateRecv = -1;
  try {
    int msg_id = _chatListStruct.sqlite->insertMessage(data);
    sent_msg.setId(QString::number(msg_id));
  } catch (const DiskError &) {
    std::stringstream stream;
    stream << "[ChatModel::insertSentMsg] Unable to write the sent message to disk.";
    logger.log(stream.str(),LogLevel::Error);
    std::exit(1);
  }
  beginInsertRows(QModelIndex(),_msgVect.size(),_msgVect.size());
  _msgVect.emplace_back(data);
  endInsertRows();
}

void ChatModel::insertRecvMsg(const QXmppMessage & recv_msg) {
  if (isValid(recv_msg)) {
    MessageStruct data;
    data.chatId = _chatListStruct.chatId;
    data.body = recv_msg.body();
    data.senderJid = _chatListStruct.recpJid;
    data.recpJid = _chatListStruct.myJid;
    data.chatId = _chatListStruct.chatId;
    data.dateSent = recv_msg.stamp().toSecsSinceEpoch();
    data.dateRecv = QDateTime::currentDateTime().toSecsSinceEpoch();
    try {
      _chatListStruct.sqlite->insertMessage(data);
    } catch (const DiskError &) {
      std::stringstream stream;
      stream << "[ChatModel::insertSentMsg] Unable to write the received message to disk.";
      logger.log(stream.str(),LogLevel::Error);
      std::exit(1);
    }
    beginInsertRows(QModelIndex(),_msgVect.size(),_msgVect.size());
    _msgVect.emplace_back(data);
    endInsertRows();
  }
}

void ChatModel::addMsgToModel(const MessageStruct & msg_struct) {
  beginInsertRows(QModelIndex(),_msgVect.size(),_msgVect.size());
  _msgVect.emplace_back(msg_struct);
  endInsertRows();
}

void ChatModel::sendMsg(QStringView body) {
  QXmppMessage msg(_chatListStruct.myJid,_chatListStruct.recpJid,body.toString());
  msg.setReceiptRequested(true);
  insertSentMsg(msg);
  _chatListStruct.sender->sendMsg(msg);
}

bool ChatModel::isValid(const QXmppMessage & recv_msg)
{
  if (recv_msg.type() == QXmppMessage::Type::Error) {
    handleErrorMsg(recv_msg);
    return false;
  } else if (recv_msg.to().split('/').first() == recv_msg.from().split('/').first()) {
    return false;
  } else {
    return true;
  }
}

int ChatModel::handleErrorMsg(const QXmppMessage & recv_msg) {
  QXmppStanza::Error error = recv_msg.error();
  logger.log("[ChatModel] " + error.text().toStdString(),LogLevel::Error);
  return error.type();
}

void SqliteForwarded::createMsgFwd(ChatModel *chat, MessageStruct &data) {
  chat->addMsgToModel(data);
}
