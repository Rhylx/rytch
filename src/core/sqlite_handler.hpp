#ifndef SQLITE_HANDLER_HPP
#define SQLITE_HANDLER_HPP

#include "../structures/contact_struct.hpp"
#include "../structures/chat_struct.hpp"
#include "../structures/message_struct.hpp"

class ChatListModel;
class ContactListModel;
class ChatModel;

struct SqliteForwarded {
  static void CreateChatFwd(ChatListModel*,ChatStruct &);
  static void createContactFwd(ContactListModel*, ContactStruct &);
  static void createMsgFwd(ChatModel *, MessageStruct &);
};

struct sqlite3;
class SqliteHandler {
  public:
    SqliteHandler(const std::string my_jid);
    SqliteHandler(const SqliteHandler&) = delete;
    ~SqliteHandler();
    SqliteHandler & operator= (const SqliteHandler&) = delete;
    std::string pathDb(const std::string my_jid) const;
    // Add
    int insertChat(const ChatStruct&) const;
    int insertContact(const ContactStruct &) const;
    int insertMessage(const MessageStruct &) const;
    // Change
    void updateSubscriptionStatus(const QString & jid, int status) const;
    void updateMsgDate(const QString & msg_id, const QString & jid, int64_t recv_date) const;
    // Read
    void extractChats(ChatListModel*) const;
    void extractContacts(ContactListModel *ptr) const;
    void extractMessages(ChatModel *chat) const;


  private:
    sqlite3 *_db;
};

#endif
