#ifndef XMPP_SENDER_HPP_INCLUDED
#define XMPP_SENDER_HPP_INCLUDED

#include <memory>
#include <QXmppClient.h>
#include <QXmppMessage.h>


class XmppSender {
  public:
    XmppSender(std::shared_ptr<QXmppClient>  r) : _client(r) {;}
    XmppSender(const XmppSender &r) : _client(r._client) {;}
    ~XmppSender(){;}
    bool sendMsg(const QXmppMessage & msg) const;
    bool sendContactRequest(const QString & bareJid) const;

  private:
    std::shared_ptr<QXmppClient> _client;
};

#endif

