#ifndef PRESENCE_HANDLER_HPP
#define PRESENCE_HANDLER_HPP

#include <QXmppClient.h>

#include "../models/contact_list_model.hpp"
#include "sqlite_handler.hpp"

class PresenceHandler : public QObject
{

  Q_OBJECT

  public:
    PresenceHandler(std::shared_ptr<QXmppClient> client,
                     std::shared_ptr<ContactListModel> contact_list,
                     std::shared_ptr<ContactListModel> request_list);
    ~PresenceHandler(){;}

    void syncWithServer();

  public slots:
    void handleRequest(const QModelIndex &index, ContactEnum::requestAction action) const;
    void handlePresence(const QXmppPresence & presence) const;
    void handleSubscriptionUpdate(const QString &bareJid) const;
    void rosterReceived();
    void contactReady();

  private:
    bool _contactReady = false;
    bool _rosterReady = false;
    std::shared_ptr<QXmppClient> _client;
    std::shared_ptr<ContactListModel> _contactList;
    std::shared_ptr<ContactListModel> _requestList;
};

#endif
