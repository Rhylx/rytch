#ifndef LOGGER_HPP
#define LOGGER_HPP

#include <string>
#include <fstream>

/// Global class to handle error messages and logs
/**
  One single instance is always available throught the execution.
  Every log, warning or error message should be written using the log function. The level distinguish between log, warning or error. 
  The higher the level, the more important the message is.
  The log level of the class determine which message should be printed, 
  only those with a level higher will be.
  */
namespace LogLevel {
  enum {
    Spam = 9,
    Info = 7,
    Warning = 5,
    Error = 3,
    Panic = 1,
  };
}
class Logger {
  public:
    Logger();
    ~Logger();
    void setLogLevel(int level) {_logLevel = level;}
    void log(const std::string &msg, int level);
    void dateStamp();
  private:
    int _logLevel;
    std::ofstream _out;
    std::string _logPath;
};

extern Logger logger;

#endif 

