#include "client_err_handler.hpp"
#include "logger.hpp"
#include <sstream>

void ClientErrHandler::manageClientError(const QXmppClient::Error &error) const {
  std::stringstream stream;
  stream<<"[ClientErrHandler] Incoming Error from the XmppClient: "<<error<<std::endl;
  logger.log(stream.str(),LogLevel::Warning);
}

void ClientErrHandler::manageSslError(const QList< QSslError > &errors) const {
  for (int i=0;i<errors.size();i++) {
    std::stringstream stream;
    stream<<"[ClientErrHandler] Incoming SSL errors from the XmppClient: "<<errors[i].error()<<std::endl;
    logger.log(stream.str(),LogLevel::Warning);
  }
}

