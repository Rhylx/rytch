#ifndef CLIENT_ERR_HANDLER_HPP
#define CLIENT_ERR_HANDLER_HPP

#include <QSslError>
#include <QObject>

#include <QXmppStanza.h>
#include <QXmppClient.h>

class ClientErrHandler : public QObject
{

  Q_OBJECT

  public:
    ClientErrHandler(){;};

  public slots:
    void manageClientError(const QXmppClient::Error&) const;
    void manageSslError(const QList<QSslError> &) const;
};

#endif
