#ifndef MESSAGE_RECEIPT_HANDLER_HPP_INCLUDED
#define MESSAGE_RECEIPT_HANDLER_HPP_INCLUDED

#include <QXmppMessageReceiptManager.h>

class MessageReceiptHandler : public QObject {

  Q_OBJECT

  public:
    MessageReceiptHandler(){;};

  public slots:
    void manageMessageDelivered(const QString & jid,const QString & id);

  private:

};

#endif

