#include "logger.hpp"

#include <fstream>
#include <filesystem>
#include <iomanip>
#include <ctime>
#include <QStandardPaths>

#define STRING(s) #s

Logger::Logger() : _logLevel(LogLevel::Warning) {
  // Find the standard location for the output
  const char* xdg_data_home = std::getenv("XDG_CACCHE_HOME");
  std::filesystem::path xdg_path;
  if (xdg_data_home == NULL) {
    xdg_path = std::filesystem::path((QStandardPaths::writableLocation(QStandardPaths::CacheLocation)).toStdString().c_str());
  } else {
    std::filesystem::path xdg_path = std::filesystem::path(xdg_data_home);
  }
  xdg_path.append("rytch");
  std::filesystem::create_directories(xdg_path);
  xdg_path.append("rytch");
  xdg_path.replace_extension(".log");

  _logPath = xdg_path.string();
  _out.open(xdg_path.string(), std::ios::out|std::ios::app);
}

Logger::~Logger() {
  _out<<"\n"<<std::endl;
  _out.close();
}

void Logger::log(const std::string &msg, int level) {
  if (_logLevel < level) return;
  {
    auto time = std::time(nullptr);
    _out << std::put_time(std::localtime(&time), "%H:%M:%S");
  }
  if (level > LogLevel::Warning) {
    _out<<" Info: ";
  } else if (level > LogLevel::Error) {
    _out<<" Warning: ";
  } else {
    _out<<" Error: ";
  }
  _out<<msg<<std::endl;
}

void Logger::dateStamp() {
  auto time = std::time(nullptr);
  _out << std::put_time(std::localtime(&time), "%Y-%m-%d-%H:%M:%S") << std::endl;
  _out << USER_AGENT << std::endl;
}

Logger logger;
