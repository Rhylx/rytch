#include "xmpp_sender.hpp"

#include <QXmppRosterManager.h>

#include "logger.hpp"
#include <sstream>

bool XmppSender::sendMsg(const QXmppMessage & msg) const {
  return _client->sendPacket(msg);
}

bool XmppSender::sendContactRequest(const QString & bareJid) const {
  std::stringstream stream;
  stream<<"[XmppSender] Subscribing to: "<<bareJid.toStdString()<<std::endl;
  logger.log(stream.str(),LogLevel::Spam);
  return _client->findExtension<QXmppRosterManager>()->subscribe(bareJid);
}
