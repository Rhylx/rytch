#include "sqlite_handler.hpp"
#include "../structures/error_types.hpp"
#include "logger.hpp"
#include <sstream>

#include <sqlite3.h>

#include <stdexcept>
#include <filesystem>
#include <QStandardPaths>


template<typename T>
void checkSQLITE_OK(int rv, const char *fname, const char *cname,bool critical) {
  if (rv != SQLITE_OK && rv != SQLITE_DONE) {
    std::stringstream stream;
    stream<< "[SqliteHandler::"<<fname<<"] "<<cname<<" failed with error code: "<<rv;
    logger.log(stream.str(),critical?LogLevel::Panic : LogLevel::Error);
    throw T("");
  }
}

SqliteHandler::SqliteHandler(const std::string my_jid) {
  int rv = sqlite3_open_v2(pathDb(my_jid).c_str(),&_db,SQLITE_OPEN_READWRITE | SQLITE_OPEN_CREATE,NULL);
  checkSQLITE_OK<DiskError>(rv,"Constructor","open_v2",true);
  rv = sqlite3_exec(_db,"CREATE TABLE IF NOT EXISTS Chats ("
                      "chat_id INTEGER PRIMARY KEY,"
                      "chat_name TEXT NOT NULL,"
                      "contact_jid TEXT NOT NULL"
                    ");",NULL,NULL,NULL);
  checkSQLITE_OK<std::runtime_error>(rv,"Constructor","CREATE TABLE Chats",true);
  rv = sqlite3_exec(_db,"CREATE TABLE IF NOT EXISTS Contacts ("
                      "contact_id INTEGER PRIMARY KEY,"
                      "contact_name TEXT NOT NULL,"
                      "contact_jid TEXT NOT NULL,"
                      "is_in_roster INTEGER NOT NULL"
                    ");",NULL,NULL,NULL);
  checkSQLITE_OK<std::runtime_error>(rv,"Constructor","CREATE TABLE Contacts",true);
  rv = sqlite3_exec(_db,"CREATE TABLE IF NOT EXISTS Messages ("
                      "message_id INTEGER PRIMARY KEY,"
                      "sender TEXT NOT NULL,"
                      "dest TEXT NOT NULL,"
                      "chat_id INTEGER NOT NULL,"
                      "body TEXT NOT NULL,"
                      "date_sent INTEGER,"
                      "date_recv INTEGER"
                    ");",NULL,NULL,NULL);
  checkSQLITE_OK<std::runtime_error>(rv,"Constructor","CREATE TABLE Messages",true);
}

SqliteHandler::~SqliteHandler() {
  sqlite3_close_v2(_db);
}

std::string SqliteHandler::pathDb(const std::string my_jid) const {
  const char* xdg_data_home = std::getenv("XDG_DATA_HOME");
  std::filesystem::path xdg_path;
  if (xdg_data_home == NULL) {
    xdg_path = std::filesystem::path((QStandardPaths::writableLocation(QStandardPaths::HomeLocation)).toStdString().c_str());
    xdg_path.append(".local");
    xdg_path.append("share");
    xdg_path.append("rytch");
    std::filesystem::create_directories(xdg_path);
  } else {
    std::filesystem::path xdg_path = std::filesystem::path(xdg_data_home);
    xdg_path.append("rytch");
    std::filesystem::create_directories(xdg_path);
  }
  std::stringstream stream;
  stream << std::hex << std::hash<std::string>{}(my_jid);
  std::string jid_hash(stream.str());
  xdg_path.append(jid_hash);
  xdg_path.replace_extension(".db");
  return xdg_path.string();
}

int SqliteHandler::insertChat(const ChatStruct &chat) const {
  sqlite3_stmt *res;
  int rv = sqlite3_exec(_db,"BEGIN IMMEDIATE TRANSACTION",NULL,NULL,NULL); // Reserve access to read new id
  checkSQLITE_OK<std::runtime_error>(rv,"insertChat","BEGIN IMMEDIATE TRANSACTION",false);
  std::string data = std::string("") + "?1,?2";
  std::string stmt = "INSERT INTO Chats (chat_name, contact_jid) VALUES (" + data + ");";
  rv = sqlite3_prepare_v2(_db, stmt.c_str(), -1, &res, 0);
  checkSQLITE_OK<std::runtime_error>(rv,"insertChat","prepare_v2",false);
  rv = sqlite3_bind_text(res,1,chat.chatLabel.toStdString().c_str(),-1,SQLITE_TRANSIENT);
  checkSQLITE_OK<std::runtime_error>(rv,"insertChat","bind_text chatLabel",false);
  rv = sqlite3_bind_text(res,2,chat.recpJid.toStdString().c_str(),-1,SQLITE_TRANSIENT);
  checkSQLITE_OK<std::runtime_error>(rv,"insertChat","bind_text recpJid",false);
  rv = sqlite3_step(res);
  checkSQLITE_OK<std::runtime_error>(rv,"insertChat","step",false);
  rv = sqlite3_finalize(res);
  checkSQLITE_OK<std::runtime_error>(rv,"insertChat","finalize",false);
  int id = sqlite3_last_insert_rowid(_db);
  {
    std::stringstream stream;
    stream<<"[SqliteHandler::insertChat] Inserting a new chat with id:"<<id;
    logger.log(stream.str(),LogLevel::Info);
  }
  rv = sqlite3_exec(_db,"END TRANSACTION",NULL,NULL,NULL);
  checkSQLITE_OK<std::runtime_error>(rv,"insertChat","END TRANSACTION",false);
  return id;
}

int SqliteHandler::insertContact(const ContactStruct &contact) const {
  sqlite3_stmt *res;
  int rv = sqlite3_exec(_db,"BEGIN IMMEDIATE TRANSACTION",NULL,NULL,NULL); // Reserve access to read new id
  checkSQLITE_OK<std::runtime_error>(rv,"insertContact","BEGIN IMMEDIATE TRANSACTION",false);
  std::string data = std::string("") + "?1,?2," +
                     std::to_string(contact.inroster);
  std::string stmt = "INSERT INTO Contacts (contact_name, contact_jid, is_in_roster)"
                     " VALUES (" + data + ");";
  rv = sqlite3_prepare_v2(_db, stmt.c_str(), -1, &res, 0);
  checkSQLITE_OK<std::runtime_error>(rv,"insertContact","prepare_v2",false);
  rv = sqlite3_bind_text(res,1,contact.name.toStdString().c_str(),-1,SQLITE_TRANSIENT);
  checkSQLITE_OK<std::runtime_error>(rv,"insertContact","bind_text name",false);
  rv = sqlite3_bind_text(res,2,contact.jid.toStdString().c_str(),-1,SQLITE_TRANSIENT);
  checkSQLITE_OK<std::runtime_error>(rv,"insertContact","bind_text jid",false);
  rv = sqlite3_step(res);
  checkSQLITE_OK<std::runtime_error>(rv,"insertContact","step",false);
  rv = sqlite3_finalize(res);
  checkSQLITE_OK<std::runtime_error>(rv,"insertContact","finalize",false);
  int id = sqlite3_last_insert_rowid(_db);
  rv = sqlite3_exec(_db,"END TRANSACTION",NULL,NULL,NULL);
  checkSQLITE_OK<std::runtime_error>(rv,"insertContact","END TRANSACTION",false);
  return id;
}

int SqliteHandler::insertMessage(const MessageStruct &msg) const {
  sqlite3_stmt *res;
  int rv = sqlite3_exec(_db,"BEGIN IMMEDIATE TRANSACTION",NULL,NULL,NULL); // Reserve access to read new id
  checkSQLITE_OK<std::runtime_error>(rv,"insertMessage","BEGIN IMMEDIATE TRANSACTION",false);
  std::string data = "?1,?2, " +
                     std::to_string(msg.chatId) + ", " +
                     std::to_string(msg.dateSent) + ", " +
                     std::to_string(msg.dateRecv) + ",?3" ;
  std::string stmt = "INSERT INTO Messages (sender, dest , chat_id, date_sent, date_recv, body) "
                     " VALUES (" + data + ");";
  rv = sqlite3_prepare_v2(_db, stmt.c_str(), -1, &res, 0);
  checkSQLITE_OK<std::runtime_error>(rv,"insertMessage","prepare_v2",false);
  rv = sqlite3_bind_text(res,1,msg.senderJid.toStdString().c_str(),-1,SQLITE_TRANSIENT);
  checkSQLITE_OK<std::runtime_error>(rv,"insertMessage","bind_text sender jid",false);
  rv = sqlite3_bind_text(res,2,msg.recpJid.toStdString().c_str(),-1,SQLITE_TRANSIENT);
  checkSQLITE_OK<std::runtime_error>(rv,"insertMessage","bind_text recp jid",false);
  rv = sqlite3_bind_text(res,3,msg.body.toStdString().c_str(),-1,SQLITE_TRANSIENT);
  checkSQLITE_OK<std::runtime_error>(rv,"insertMessage","bind_text body",false);
  rv = sqlite3_step(res);
  checkSQLITE_OK<std::runtime_error>(rv,"insertMessage","step",false);
  rv = sqlite3_finalize(res);
  checkSQLITE_OK<std::runtime_error>(rv,"insertMessage","finalize",false);
  int id = sqlite3_last_insert_rowid(_db);
  rv = sqlite3_exec(_db,"END TRANSACTION",NULL,NULL,NULL);
  checkSQLITE_OK<std::runtime_error>(rv,"insertMessage","END TRANSACTION",false);
  return id;
}

void SqliteHandler::updateSubscriptionStatus(const QString & jid,int status) const {
  sqlite3_stmt *res;
  int rv = sqlite3_exec(_db,"BEGIN IMMEDIATE TRANSACTION",NULL,NULL,NULL);
  checkSQLITE_OK<std::runtime_error>(rv,"updateSubscriptionStatus","BEGIN IMMEDIATE TRANSACTION",false);
  std::string stmt = "UPDATE Contacts SET is_in_roster = " + std::to_string(status) +
                     " WHERE contact_jid = ?1;";
  rv = sqlite3_prepare_v2(_db, stmt.c_str(), -1, &res, 0);
  checkSQLITE_OK<std::runtime_error>(rv,"updateSubscriptionStatus","prepare_v2",false);
  rv = sqlite3_bind_text(res,1,jid.toStdString().c_str(),-1,SQLITE_TRANSIENT);
  checkSQLITE_OK<std::runtime_error>(rv,"updateSubscriptionStatus","bind_text jid",false);
  rv = sqlite3_step(res) != SQLITE_DONE;
  checkSQLITE_OK<std::runtime_error>(rv,"updateSubscriptionStatus","step",false);
  rv = sqlite3_finalize(res);
  checkSQLITE_OK<std::runtime_error>(rv,"updateSubscriptionStatus","finalize",false);
  rv = sqlite3_exec(_db,"END TRANSACTION",NULL,NULL,NULL);
  checkSQLITE_OK<std::runtime_error>(rv,"updateSubscriptionStatus","END TRANSACTION",false);
}

void SqliteHandler::updateMsgDate(const QString & msg_id, const QString & jid, int64_t recv_date) const {
  sqlite3_stmt *res;
  int rv = sqlite3_exec(_db,"BEGIN IMMEDIATE TRANSACTION",NULL,NULL,NULL);
  checkSQLITE_OK<std::runtime_error>(rv,"updateMsgDate","BEGIN IMMEDIATE TRANSACTION",false);
  std::string stmt = "UPDATE Messages SET date_recv = " + std::to_string(recv_date) +
                     " WHERE message_id = ?1 AND dest = ?2 AND date_recv = -1;";
  rv = sqlite3_prepare_v2(_db, stmt.c_str(), -1, &res, 0);
  checkSQLITE_OK<std::runtime_error>(rv,"updateMsgDate","prepare_v2",false);
  rv = sqlite3_bind_text(res,1,msg_id.toStdString().c_str(),-1,SQLITE_TRANSIENT);
  checkSQLITE_OK<std::runtime_error>(rv,"updateMsgDate","bind_text msg_id",false);
  rv = sqlite3_bind_text(res,2,jid.toStdString().c_str(),-1,SQLITE_TRANSIENT);
  checkSQLITE_OK<std::runtime_error>(rv,"updateMsgDate","bind_text jid",false);
  rv = sqlite3_step(res) != SQLITE_DONE;
  checkSQLITE_OK<std::runtime_error>(rv,"updateMsgDate","step",false);
  rv = sqlite3_finalize(res);
  checkSQLITE_OK<std::runtime_error>(rv,"updateMsgDate","finalize",false);
  rv = sqlite3_exec(_db,"END TRANSACTION",NULL,NULL,NULL);
  checkSQLITE_OK<std::runtime_error>(rv,"updateMsgDate","END TRANSACTION",false);
}

void SqliteHandler::extractChats(ChatListModel *ptr) const {
  sqlite3_stmt *res;
  std::string stmt = "SELECT chat_id, chat_name, Chats.contact_jid FROM Chats INNER JOIN Contacts ON Contacts.contact_jid = Chats.contact_jid ;";
  int rv = sqlite3_prepare_v2(_db, stmt.c_str(), -1, &res, 0);
  checkSQLITE_OK<std::runtime_error>(rv,"extractChats","prepare_v2",false);
  int rc = sqlite3_step(res);
  while (rc == SQLITE_ROW) {
    ChatStruct data;
    data.chatId = sqlite3_column_int(res,0);
    data.chatLabel = reinterpret_cast<const char *>(sqlite3_column_text(res,1));
    data.recpJid = reinterpret_cast<const char *>(sqlite3_column_text(res,2));
    try{
      SqliteForwarded::CreateChatFwd(ptr, data);
    } catch(const std::runtime_error &){
      std::stringstream stream;
      stream << "[SqliteHandler::extractChats] Non unique Id retrieved in the chat list. Offending jid: " << data.chatId;
      stream << " Database probably corructed, aborting.";
      logger.log(stream.str(),LogLevel::Panic);
      std::exit(1);
    }
    rc = sqlite3_step(res);
  }
  rv = sqlite3_finalize(res);
  checkSQLITE_OK<std::runtime_error>(rv,"extractChats","finalize",false);
}

void SqliteHandler::extractContacts(ContactListModel *ptr) const {
  sqlite3_stmt *res;
  std::string stmt = "SELECT * FROM Contacts;";
  int rv = sqlite3_prepare_v2(_db, stmt.c_str(), -1, &res, 0);
  checkSQLITE_OK<std::runtime_error>(rv,"extractContacts","prepare_v2",false);
  int rc = sqlite3_step(res);
  while (rc == SQLITE_ROW) {
    const char* contact_name = reinterpret_cast<const char *>(sqlite3_column_text(res,1));
    const char* contact_jid = reinterpret_cast<const char *>(sqlite3_column_text(res,2));
    int is_in_roster = sqlite3_column_int(res,3);
    ContactStruct mystruct;
    mystruct.jid = contact_jid;
    mystruct.name = contact_name;
    mystruct.inroster = static_cast<ContactEnum::SubscriptionStatus>(is_in_roster);
    try{
      SqliteForwarded::createContactFwd(ptr, mystruct);
    } catch(const std::runtime_error &){
      std::stringstream stream;
      stream << "[SqliteHandler::extractContacts] Non unique Jid retrieved in the contact list. Offending jid: " << contact_jid;
      stream << " Database probably corructed, aborting.";
      logger.log(stream.str(),LogLevel::Panic);
      std::exit(1);
    }
    rc = sqlite3_step(res);
  }
  rv = sqlite3_finalize(res);
  checkSQLITE_OK<std::runtime_error>(rv,"extractContacts","finalize",false);
}

void SqliteHandler::extractMessages(ChatModel* chat) const {
  sqlite3_stmt *res;
  std::string stmt = "SELECT * FROM Messages where chat_id="+std::to_string(chat->id())+";";
  int rv = sqlite3_prepare_v2(_db, stmt.c_str(), -1, &res, 0);
  checkSQLITE_OK<std::runtime_error>(rv,"extractMessages","prepare_v2",false);
  int rc = sqlite3_step(res);
  while (rc == SQLITE_ROW) {
    const int mid = sqlite3_column_int(res,0);
    const char* sender_jid = reinterpret_cast<const char *>(sqlite3_column_text(res,1));
    const char* dest_jid = reinterpret_cast<const char *>(sqlite3_column_text(res,2));
    const int chat_id = sqlite3_column_int(res,3);
    const char* body = reinterpret_cast<const char *>(sqlite3_column_text(res,4));
    const int date_sent = sqlite3_column_int(res,5);
    const int date_recv = sqlite3_column_int(res,6);
    MessageStruct mstruct;
    mstruct.messageId=mid;
    mstruct.senderJid=sender_jid;
    mstruct.recpJid=dest_jid;
    mstruct.chatId=chat_id;
    mstruct.body=body;
    mstruct.dateSent=date_sent;
    mstruct.dateRecv=date_recv;
    SqliteForwarded::createMsgFwd(chat, mstruct); // Would only fail on a bad alloc
    rc = sqlite3_step(res);
  }
  rv = sqlite3_finalize(res);
  checkSQLITE_OK<std::runtime_error>(rv,"extractMessages","finalize",false);
}
