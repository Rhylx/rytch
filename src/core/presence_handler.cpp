#include "presence_handler.hpp"

#include <QXmppRosterManager.h>
#include "../structures/error_types.hpp"

#include <sstream>
#include "logger.hpp"

using namespace ContactEnum;

PresenceHandler::PresenceHandler(std::shared_ptr<QXmppClient> client,
                                   std::shared_ptr<ContactListModel> contact_list,
                                   std::shared_ptr<ContactListModel> request_list) :
  _client(client),
  _contactList(contact_list),
  _requestList(request_list) {
  connect(_contactList.get(), &ContactListModel::initialized, this, &PresenceHandler::contactReady);
  connect(_client->findExtension<QXmppRosterManager>(), &QXmppRosterManager::rosterReceived, this, &PresenceHandler::rosterReceived);
  connect(_client.get(), &QXmppClient::presenceReceived, this, &PresenceHandler::handlePresence);
  connect(_client->findExtension<QXmppRosterManager>(), &QXmppRosterManager::itemAdded, this, &PresenceHandler::handleSubscriptionUpdate);
  connect(_client->findExtension<QXmppRosterManager>(), &QXmppRosterManager::itemChanged, this, &PresenceHandler::handleSubscriptionUpdate);
  connect(_client->findExtension<QXmppRosterManager>(), &QXmppRosterManager::itemRemoved, this, &PresenceHandler::handleSubscriptionUpdate);
}

void PresenceHandler::syncWithServer() {
  assert(_client->findExtension<QXmppRosterManager>()->isRosterReceived());
  QStringList jid_list = _client->findExtension<QXmppRosterManager>()->getRosterBareJids();
  _contactList->getContactBareJids(jid_list);
  for(auto const &jid : jid_list) {
    handleSubscriptionUpdate(jid);
  }
}

void PresenceHandler::handleRequest(const QModelIndex &index, requestAction action) const {
  QString contact_jid = index.data(jid).value<QString>();
  _requestList->removeEntry(index.row());
  switch(action) {
    case (Accept):
      _client->findExtension<QXmppRosterManager>()->acceptSubscription(contact_jid);
      _contactList->tryAddContact(contact_jid,contact_jid.split('@').first(),From);
      break;
    case (Deny):
      _client->findExtension<QXmppRosterManager>()->refuseSubscription(contact_jid);
      break;
    default:
      throw ArgNotInEnum("") ;
  }
}

void PresenceHandler::handlePresence(const QXmppPresence & presence) const {
  switch(presence.type()) {
    case(QXmppPresence::Type::Error):
      {
        std::stringstream stream;
        stream <<"[PresenceHandler::handlePresence] Error received in presence: "<<presence.error().text().toStdString()<<std::endl;
        logger.log(stream.str(),LogLevel::Warning);
      }
      break;
    case(QXmppPresence::Type::Available):
      {
        std::stringstream stream;
        stream << "[PresenceHandler::handlePresence] Received presence from: "<<presence.from().toStdString()<<", status: "<<presence.availableStatusType()<<" : "<<presence.statusText().toStdString();
        logger.log(stream.str(),LogLevel::Spam);
      }
      // Convert from QXmpp to ours
      AvailableStatus status;
      switch(presence.availableStatusType()) {
        case(QXmppPresence::AvailableStatusType::Online):
          status = Online;
          break;
        case(QXmppPresence::AvailableStatusType::Away):
          status = Away;
          break;
        case(QXmppPresence::AvailableStatusType::XA):
          status = XA;
          break;
        case(QXmppPresence::AvailableStatusType::DND):
          status = DND;
          break;
        case(QXmppPresence::AvailableStatusType::Chat):
          status = Chat;
          break;
        default:
          status = Unknown;
          break;
      } // end convert
      _contactList->setAvailableStatus(presence.from().split('/').first(),status);
      break;
    case(QXmppPresence::Type::Unavailable):
      _contactList->setAvailableStatus(presence.from().split('/').first(),AvailableStatus::Offline);
      break;
    default:
      break;
  }
}

void PresenceHandler::handleSubscriptionUpdate(const QString &bareJid) const {
  SubscriptionStatus status;
  switch(_client->findExtension<QXmppRosterManager>()->getRosterEntry(bareJid).subscriptionType()) {
    case(QXmppRosterIq::Item::SubscriptionType::None):
      status = None;
      break;
    case(QXmppRosterIq::Item::SubscriptionType::From):
      status = From;
      break;
    case(QXmppRosterIq::Item::SubscriptionType::To):
      status = To;
      break;
    case(QXmppRosterIq::Item::SubscriptionType::Both):
      status = Both;
      break;
    default:
      status = None;
      break;
  }
  _contactList->setSubscriptionStatus(bareJid,status);
}

void PresenceHandler::rosterReceived() {
  _rosterReady = true;
  if (_contactReady) syncWithServer();
}

void PresenceHandler::contactReady() {
  _contactReady = true;
  if (_rosterReady) syncWithServer();
}

