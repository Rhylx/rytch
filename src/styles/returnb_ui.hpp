#ifndef RETURNB_UI_HPP
#define RETURNB_UI_HPP

#include <QPainter>
#include <QPushButton>

class ReturnButtonUi : public QPushButton {
  public:
    ReturnButtonUi(QWidget *parent=nullptr): QPushButton(parent){;};
    ~ReturnButtonUi(){;};

  private:
    void paintEvent(QPaintEvent* event) override;
};

#endif
