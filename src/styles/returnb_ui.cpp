#include "returnb_ui.hpp"

#include "../styles/global_style.hpp"

void ReturnButtonUi::paintEvent(QPaintEvent* event) {
  Q_UNUSED(event);
  QPainter painter(this);
  painter.setRenderHint(QPainter::Antialiasing, true);
  painter.setPen(Qt::NoPen);
  painter.setBrush(QColor(Style::ChatStyle::returnBC.backgroundColor));
  int r_width = parentWidget()->size().width();
  int r_height = parentWidget()->size().height();
  double unit_w = r_width/80.;
  double unit_h = r_height/80.;
  painter.drawEllipse(QRect(-2.5*unit_w,-2.5*unit_h,5*unit_w,5*unit_h));
}
