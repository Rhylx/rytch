#ifndef ROUNDLISTVIEW_HPP
#define ROUNDLISTVIEW_HPP
#include <QPainter>
#include <QListView>

/// Is the specialisation of QListView with border-radius.
class RoundListView : public QListView {
  public:
    RoundListView(QWidget *parent=nullptr);
    ~RoundListView(){;};

    void setRadius(unsigned int r) {_radius = r;}

  private:
    void paintEvent(QPaintEvent* event) override;
    unsigned int _radius = 0;
};

#endif
