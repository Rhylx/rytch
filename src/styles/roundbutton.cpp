#include "roundbutton.hpp"

#include <QStyleOption>
#include <QPainterPath>
#include <QPen>
#include <QPalette>

void RoundButton::paintEvent(QPaintEvent* event) {
  QPalette palette=this->palette();
  QPainter p(this);
  QPainterPath borderPath;
  QColor bColor=palette.color(QPalette::Button);
  p.setPen(QPen(_borderColor, 2));
  p.setRenderHint(QPainter::Antialiasing, true);
  QRect buttonRect = rect().adjusted(1, 1, -1, -1);
  borderPath.addRoundedRect(buttonRect,_radius,_radius);
  p.fillPath(borderPath, bColor);
  p.drawPath(borderPath);
  p.setPen(_borderColor);
  p.drawText(buttonRect, Qt::AlignCenter, text());
}
