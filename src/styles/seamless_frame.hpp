#ifndef SEAMLESS_HPP_INCLUDED
#define SEAMLESS_HPP_INCLUDED

#include <QFrame>

/// Provide a container without border that forward loss of focus
class SeamlessFrame : public QFrame {

  Q_OBJECT

  public:
    SeamlessFrame(QWidget *parent = nullptr);

    void setCloseOnLost(bool on = true);

  signals:
    void lostFocus(SeamlessFrame *);

  protected:
    void showEvent(QShowEvent *event) override;

  private slots:
    void updateFocus(QWidget *old, QWidget *now);

  private:
    int _focusTracker;
    bool _closeOnLost = false;
};

#endif

