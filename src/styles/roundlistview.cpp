#include "roundlistview.hpp"
#include "global_style.hpp"

#include <QStyleOption>
#include <QPainterPath>
#include <QPen>
#include <QPalette>

RoundListView::RoundListView(QWidget *parent): QListView(parent) {
  viewport()->setAutoFillBackground(false);
  setFrameStyle(QFrame::NoFrame);
}

void RoundListView::paintEvent(QPaintEvent* event) {
  QPalette palette=this->palette();
  QPainter p(this->viewport());
  p.setRenderHint(QPainter::Antialiasing, true);
  QPainterPath borderPath;
  QColor bColor=palette.color(QPalette::Window);
  p.setPen(Qt::NoPen);
  p.setRenderHint(QPainter::Antialiasing, true);
  borderPath.addRoundedRect(rect().adjusted(1, 1, -1, -1), _radius, _radius);
  p.fillPath(borderPath, bColor);
  QListView::paintEvent(event);
}
