#ifndef ROUNDLINEEDIT_HPP
#define ROUNDLINEEDIT_HPP
#include <QPainter>
#include <QLineEdit>
#include <QMargins>

/// Is the specialisation of QLineEdit with border-radius.
class RoundLineEdit : public QLineEdit {
  public:
    RoundLineEdit(QWidget *parent=nullptr);
    ~RoundLineEdit(){;};

  private:
    void paintEvent(QPaintEvent* event) override;
    QMargins effectiveTextMargins() const;
};

#endif
