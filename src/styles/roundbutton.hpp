#ifndef ROUNDBUTTON_HPP
#define ROUNDBUTTON_HPP
#include <QPainter>
#include <QPushButton>

/// Is the specialisation of QPushButton with border-radius.
class RoundButton : public QPushButton {
  public:
    RoundButton(QWidget *parent=nullptr): QPushButton(parent){;};
    RoundButton(const QString &text, QWidget *parent=nullptr): QPushButton(text, parent){;};
    ~RoundButton(){;};

    void setRadius(unsigned int r) {_radius = r;}

  private:
    void paintEvent(QPaintEvent* event) override;
    QColor _borderColor = Qt::black;
    unsigned int _radius = 0;
};

#endif
