#ifndef GLOBAL_STYLE_HPP_INCLUDED
#define GLOBAL_STYLE_HPP_INCLUDED

#include <QPen>
#include <QFont>
#include <QColor>

/**
Note: We must use QColor instead of int, because the constructor of QColor from an int discard the alpha value
  */
struct ColorStyle {
  QColor backgroundColor;
  QColor textColor;
  QColor borderColor;
};

struct ShapeStyle {
  unsigned int borderRadius; // in pixels
  unsigned int borderSize; // in pixels
  Qt::PenStyle borderStyle;
};

struct TextStyle {
  QFont::Weight fontWeight;
  int           pixelSize;
};

struct WidgetStyle {
  ColorStyle CS;
  ShapeStyle SS;
  TextStyle  TS;
};

struct AvailableStatusDisplay {
  QColor onlineColor;
  QColor offlineColor;
  QColor awayColor;
  QColor xaColor;
  QColor dndColor;
  QColor chatColor;
  QColor unknownColor;
};

namespace Style {
  namespace ChatStyle {
    extern ColorStyle returnBC;
    extern WidgetStyle sendB;
    extern ColorStyle ChatUi;
  }
  namespace HomeStyle {
    extern WidgetStyle chatListCont;
    extern WidgetStyle addChat;
    extern WidgetStyle chatList;
    extern WidgetStyle contactListCont;
    extern WidgetStyle addContact;
    extern WidgetStyle contactList;
    extern ColorStyle  HomeUi;
  }
  namespace ContactListStyle {
    extern AvailableStatusDisplay status;
    extern WidgetStyle item;
    extern ColorStyle sendRequest;
  }
  namespace ChatListStyle {
    extern WidgetStyle item;
  }
  namespace LoginStyle {
    extern WidgetStyle qLineEdit;
    extern WidgetStyle qPushButton;
    extern ColorStyle LoginUi;
  }
  void setDefault();
}

#endif
