#include "seamless_frame.hpp"

#include <QApplication>
#include <QShowEvent>

SeamlessFrame::SeamlessFrame(QWidget *parent) :
  QFrame(parent) {
    setFocusPolicy(Qt::ClickFocus);
    connect(qApp,&QApplication::focusChanged,this,&SeamlessFrame::updateFocus);
    setStyleSheet("background-color: #172578");
    setLineWidth(4);
    setFrameStyle(QFrame::Box);
}

void SeamlessFrame::setCloseOnLost(bool on) {
  _closeOnLost = on;
}

void SeamlessFrame::showEvent(QShowEvent *event) {
  if (!(event->spontaneous())) {
    // Set default, non spontaneous -> call by the application
    int r_width = parentWidget()->size().width();
    int r_height = parentWidget()->size().height();
    this->setGeometry(r_width*0.4,r_height*0.4,200,100);
  }
  QWidget::showEvent(event);
}

void SeamlessFrame::updateFocus(QWidget *old, QWidget *now) {
  Q_UNUSED(old);
  if (now == this) return;
  for (QObject *child : children()) {
    if (now == child) return;
  }
  emit this->lostFocus(this);
  if (_closeOnLost) {
    close();
  }
}
