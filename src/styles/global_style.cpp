#include "global_style.hpp"

namespace Style {
  ColorStyle ChatStyle::returnBC;
  WidgetStyle ChatStyle::sendB;
  ColorStyle ChatStyle::ChatUi;
  AvailableStatusDisplay ContactListStyle::status;
  WidgetStyle ContactListStyle::item;
  ColorStyle ContactListStyle::sendRequest;
  WidgetStyle ChatListStyle::item;
  WidgetStyle LoginStyle::qLineEdit;
  WidgetStyle LoginStyle::qPushButton;
  ColorStyle LoginStyle::LoginUi;
  WidgetStyle HomeStyle::chatListCont;
  WidgetStyle HomeStyle::addChat;
  WidgetStyle HomeStyle::chatList;
  WidgetStyle HomeStyle::contactListCont;
  WidgetStyle HomeStyle::addContact;
  WidgetStyle HomeStyle::contactList;
  ColorStyle HomeStyle::HomeUi;

  void setReturnBC() {
    ChatStyle::returnBC.backgroundColor.setRgba(0xffae40f2);
  }

  void setContact() {
    ContactListStyle::status.onlineColor.setRgba(0xff368600);
    ContactListStyle::status.offlineColor.setRgba(0xffa8b8d9);
    ContactListStyle::status.awayColor.setRgba(0xffd2cf00);
    ContactListStyle::status.xaColor.setRgba(0xff000000);
    ContactListStyle::status.dndColor.setRgba(0xffbb0404);
    ContactListStyle::status.chatColor.setRgba(0xff71ff8e);
    ContactListStyle::status.unknownColor.setRgba(0xff000000);
    ContactListStyle::item.CS.backgroundColor.setRgba(0xffae40f2);
    ContactListStyle::item.CS.textColor.setRgba(0xff000000);
    ContactListStyle::item.SS.borderRadius = 15;
    ContactListStyle::sendRequest.backgroundColor.setRgba(0xff2034a8);
  }

  void setChatList() {
    ChatListStyle::item.CS.backgroundColor.setRgba(0xffea40f2);
    ChatListStyle::item.CS.textColor.setRgba(0xff000000);
    ChatListStyle::item.SS.borderRadius = 15;
  }

  void setLogin() {
    LoginStyle::qLineEdit.CS.backgroundColor.setRgba(0xff0f8a8a);
    LoginStyle::qLineEdit.CS.textColor.setRgba(0xff000000);
    LoginStyle::qLineEdit.CS.borderColor.setRgba(0xff000000);
    LoginStyle::qLineEdit.SS.borderRadius = 15;
    LoginStyle::qLineEdit.SS.borderSize = 3;
    LoginStyle::qLineEdit.SS.borderStyle = Qt::SolidLine;
    LoginStyle::qLineEdit.TS.fontWeight = QFont::Bold;
    LoginStyle::qLineEdit.TS.pixelSize = 18;
    LoginStyle::qPushButton.CS.backgroundColor.setRgba(0xff0f8a8a);
    LoginStyle::qPushButton.CS.textColor.setRgba(0xff000000);
    LoginStyle::qPushButton.CS.borderColor.setRgba(0xff000000);
    LoginStyle::qPushButton.SS.borderRadius = 15;
    LoginStyle::qPushButton.SS.borderSize = 3;
    LoginStyle::qPushButton.SS.borderStyle = Qt::SolidLine;
    LoginStyle::qPushButton.TS.fontWeight = QFont::Bold;
    LoginStyle::qPushButton.TS.pixelSize = 16;
    LoginStyle::LoginUi.backgroundColor.setRgba(0xff0d537e);
  }

  void setHome() {
    HomeStyle::chatListCont.CS.backgroundColor.setRgba(0xff172578);
    HomeStyle::chatListCont.SS.borderRadius = 15;
    HomeStyle::contactListCont.CS.backgroundColor.setRgba(0xff2034a8);
    HomeStyle::contactListCont.SS.borderRadius = 15;
    HomeStyle::chatList.CS.backgroundColor.setRgba(0x26B1B2B5);
    HomeStyle::addChat.CS.backgroundColor.setRgba(0xffa83eea);
    HomeStyle::addChat.SS.borderRadius = 15;
    HomeStyle::contactList = HomeStyle::chatList;
    HomeStyle::addContact = HomeStyle::addChat;
    HomeStyle::HomeUi.backgroundColor.setRgba(0xff000000);
  }

  void setChat() {
    ChatStyle::ChatUi.backgroundColor.setRgba(0xff172578);
    ChatStyle::sendB.CS.backgroundColor.setRgba(0xff0f8a8a);
    ChatStyle::sendB.CS.textColor.setRgba(0xff000000);
    ChatStyle::sendB.SS.borderRadius = 15;
  }

  void setDefault() {
    setReturnBC();
    setContact();
    setChatList();
    setChat();
    setLogin();
    setHome();
  }
}
