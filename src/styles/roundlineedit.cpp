#include "roundlineedit.hpp"
#include "global_style.hpp"

#include <QStyleOption>
#include <QPainterPath>
#include <QPen>
#include <QPalette>
#include <QTextLayout>
#include <QTextCursor>

RoundLineEdit::RoundLineEdit(QWidget *parent) :
 QLineEdit(parent) {
   this->setFrame(false);
   this->setAlignment(Qt::AlignCenter);
}

void RoundLineEdit::paintEvent(QPaintEvent* event) {
  QPalette palette=this->palette();
  QPainter p(this);
  QPainterPath borderPath;
  QColor bColor=palette.color(QPalette::Window);
  palette.setColor(QPalette::Base, Qt::transparent);
  this->setPalette(palette);
  p.setPen(QPen(Style::LoginStyle::qLineEdit.CS.textColor, 2));
  p.setRenderHint(QPainter::Antialiasing, true);
  QRect buttonRect = rect().adjusted(1, 1, -1, -1);
  borderPath.addRoundedRect(buttonRect, Style::LoginStyle::qLineEdit.SS.borderRadius, Style::LoginStyle::qLineEdit.SS.borderRadius);
  p.fillPath(borderPath, bColor);
  p.drawPath(borderPath);
  QLineEdit::paintEvent(event);
}
