# Rytch

Xmpp client written in c++ based on:
  - qt c++ : https://www.qt.io/
  - qxmpp library : https://doc.qxmpp.org/qxmpp-1/
  - sqlite3 : https://www.sqlite.org/index.html

# Features of the alpha

In the alpha version one is able to log on a xmpp server, add and chat with people which have an xmpp account.
The contacts, chats and messages are locally stored in a sqlite3 database.


# Future development
