cmake_minimum_required(VERSION 3.14)
project(Rytch VERSION 0.0.9)

set(CMAKE_CXX_STANDARD 17)

IF (WIN32)
  SET(CMAKE_CXX_FLAGS "-std=c++${CMAKE_CXX_STANDARD} -g -Wall ")
ELSE()
  SET(CMAKE_CXX_FLAGS "-std=c++${CMAKE_CXX_STANDARD} -g -Wall -Wpedantic -Werror")
ENDIF()
SET(CMAKE_CXX_FLAGS_DEBUGALL "${CMAKE_CXX_FLAGS} -Og -DDEBUG -fsanitize=address,undefined -pthread ${ADDITIONAL_FLAGS}" )
SET(CMAKE_CXX_FLAGS_DEBUG "${CMAKE_CXX_FLAGS} -O3 -DOPTIMIZE -DDEBUG -pthread ${ADDITIONAL_FLAGS}" )
SET(CMAKE_CXX_FLAGS_RELEASE "${CMAKE_CXX_FLAGS} -O3 -DNDEBUG -DOPTIMIZE -pthread ${ADDITIONAL_FLAGS}" )
SET(CMAKE_CXX_FLAGS_RELWITHDEBINFO "${CMAKE_CXX_FLAGS} -O3 -g -DNDEBUG -DOPTIMIZE -pthread ${ADDITIONAL_FLAGS}" )

#SET(CMAKE_BUILD_TYPE Debug)
#SET(CMAKE_BUILD_TYPE Debugall)
IF(NOT CMAKE_BUILD_TYPE)
  SET(CMAKE_BUILD_TYPE RelWithDebInfo CACHE STRING "Choose the type of build: Debug Release RelWithDebInfo" FORCE)
ENDIF(NOT CMAKE_BUILD_TYPE)
MESSAGE(STATUS "Build type : ${CMAKE_BUILD_TYPE}")

set(CMAKE_AUTOMOC ON)
set(CMAKE_AUTORCC ON)

IF(WIN32)
  set(SQLite3_INCLUDE_DIR "C:/Users/Default/compile_rytch/sqlite3-source/")
  set(SQLite3_LIBRARY "C:/Users/Default/compile_rytch/sqlite3.lib")
ENDIF()

find_package(Qt5 REQUIRED COMPONENTS Core Network Xml Widgets)
find_package(QXmpp REQUIRED)
find_package(SQLite3 REQUIRED)

SET(EXTERNAL_INCLUDES ${EXTERNAL_INCLUDES} ${SQLite3_INCLUDE_DIRS})
SET(EXTERNAL_LIBRARIES ${EXTERNAL_LIBRARIES} ${SQLite3_LIBRARIES})

# Define user agent
add_compile_definitions(USER_AGENT="${CMAKE_SYSTEM_NAME}-${CMAKE_SYSTEM_VERSION} ${PROJECT_NAME} v${PROJECT_VERSION}")

# Target
FILE(GLOB source_list src/ui/*.cpp src/core/*.cpp src/delegates/*.cpp src/views/*.cpp src/models/*.cpp src/styles/*.cpp)
add_executable(${PROJECT_NAME} src/main.cpp ${source_list})
target_link_libraries(${PROJECT_NAME} Qt::Core Qt::Widgets QXmpp::QXmpp ${EXTERNAL_LIBRARIES})

# install
INSTALL(TARGETS ${PROJECT_NAME} DESTINATION bin)

# source tar
ADD_CUSTOM_TARGET(create_tar COMMAND
  "tar" "--exclude=*.swp" "-C" "${CMAKE_SOURCE_DIR}"
        "-vcaf" "rytch_${CMAKE_PROJECT_VERSION}.orig.tar.gz"
        "doc/" "src/" "visualization/" "CMakeLists.txt" "README.md")

